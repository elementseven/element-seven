<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Home
Route::get('/', [PageController::class, 'welcome'])->name('welcome');

// Work
Route::get('/work', [PageController::class, 'work'])->name('work.index');
Route::get('/work/{slug}', [PageController::class, 'workShow'])->name('work.show');

// Blog
Route::get('/blog', [PageController::class, 'blog'])->name('blog.index');
Route::get('/blog/{date}/{slug}', [PageController::class, 'blogShow'])->name('blog.show');

// Services
Route::get('/services', [PageController::class, 'services'])->name('services.index');
Route::get('/services/websites', [PageController::class, 'servicesWebsites'])->name('services.websites');
Route::get('/services/e-commerce', [PageController::class, 'servicesEcommerce'])->name('services.ecommerce');
Route::get('/services/social-media', [PageController::class, 'servicesSocialmedia'])->name('services.socialmedia');
Route::get('/services/aftercare', [PageController::class, 'servicesAftercare'])->name('services.aftercare');
Route::get('/services/seo', [PageController::class, 'servicesSeo'])->name('services.seo');

// Contact
Route::get('/contact', [PageController::class, 'contact'])->name('contact');

// Landing Pages
Route::get('/what-we-do/{slug}', [PageController::class, 'landingPage'])->name('what-we-do.landing-page');

// Terms & Conditions
Route::get('/tandcs/', [PageController::class, 'tandcs'])->name('tandcs');
Route::get('/tandcs-28-10-2019', [PageController::class, 'tandcsv1'])->name('tandcsv1');


// Dynamic Content
Route::get('/fetch/blog/get', [PageController::class, 'getPosts'])->name('blog.get');
Route::get('/fetch/blog/get-categories', [PageController::class, 'getCategories'])->name('blog.get.categories');
Route::get('/fetch/blog/{date}/{slug}', [PageController::class, 'showBlog'])->name('blog.show');
Route::get('/fetch/work/get', [PageController::class, 'getProjects'])->name('work.get');
Route::get('/fetch/work/get-categories', [PageController::class, 'getWorkCategories'])->name('work.get.categories');
Route::get('/fetch/work/{slug}', [PageController::class, 'showWork'])->name('work.show');
Route::get('/fetch/landingpage/{slug}', [PageController::class, 'showLP'])->name('landingpage.show');


// Redirects
Route::get('/website-builders', function(){
    return redirect()->to('/what-we-do/website-builders');
})->name('website-builders');
Route::get('/websites', function(){
    return redirect()->to('/what-we-do/websites');
})->name('websites');
Route::get('/fitness-websites', function(){
    return redirect()->to('/what-we-do/fitness-websites');
})->name('fitness-websites');
Route::get('/tourism-websites', function(){
    return redirect()->to('/what-we-do/tourism-websites');
})->name('tourism-websites');
Route::get('/websites-for-startups', function(){
    return redirect()->to('/what-we-do/websites-for-startups');
})->name('websites-for-startups');
Route::get('/startup-grant', function(){
    return redirect()->to('/what-we-do/websites-for-startups');
})->name('startup-grant');
Route::get('/website-design-belfast', function(){
    return redirect()->to('/what-we-do/website-design-belfast');
})->name('website-design-belfast');

// Post Routes
Route::post('/send-message', [SendMail::class, 'enquiry'])->name('enquiry');

// Auth Routes
Auth::routes();
