<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
class Project extends Model implements HasMedia
{
    use HasFactory;
  use InteractsWithMedia;

    protected $fillable = [
      'title','slug','status','exerpt','body','category_id','created_at'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($project) {
            $project->slug = Str::slug($project->title, "-");
        });
    }

    public function registerMediaConversions(Media $media = null): void
  {
       	$this->addMediaConversion('normal')->width(1170);
        $this->addMediaConversion('normal-webp')->width(1170)->format('webp');
        $this->addMediaConversion('double')->width(2340);
        $this->addMediaConversion('double-webp')->width(2340)->format('webp');
        $this->addMediaConversion('thumbnail')->crop('crop-center', 400, 267);
        $this->addMediaConversion('featured')->crop('crop-center', 800, 534);
        $this->addMediaConversion('featured-webp')->crop('crop-center', 800, 534)->format('webp');
    }

    public function registerMediaCollections(): void
{
        $this->addMediaCollection('projects')->singleFile();
    }
    
    public function category(){
      return $this->belongsTo('App\Models\Category');
    }
    public function pictures(){
      return $this->hasMany('App\Models\Picture');
    }
    public function landingpages(){
        return $this->belongsToMany('App\Models\LandingPage', 'landing_page_project')->withPivot('landing_page_id');
      }
}
