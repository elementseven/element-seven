<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Image extends Model implements HasMedia
{
	use HasFactory;
  use InteractsWithMedia;

	public function registerMediaConversions(Media $media = null): void
  {
  	$this->addMediaConversion('double')->width(2220);
    $this->addMediaConversion('double-webp')->width(2220)->format('webp');
    $this->addMediaConversion('normal')->width(1110);
    $this->addMediaConversion('normal-webp')->width(1110)->format('webp');
    $this->addMediaConversion('mobile')->width(300);
    $this->addMediaConversion('mobile-webp')->width(300)->format('webp');
  }
  public function post(){
      return $this->belongsTo('App\Models\Post');
  }
}

