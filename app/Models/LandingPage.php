<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class LandingPage extends Model implements HasMedia
{
  use HasFactory;
  use InteractsWithMedia;

  protected $fillable = [
    'title','slug','status','exerpt','body','category_id'
  ];

  protected static function boot()
  {
      parent::boot();
      static::saving(function ($landingpage) {
          $landingpage->slug = Str::slug($landingpage->title, "-"); 
      });
  }

  public function registerMediaConversions(Media $media = null): void
  {   
     	$this->addMediaConversion('normal')->width(1170);
      $this->addMediaConversion('normal-webp')->width(1170)->format('webp');
      $this->addMediaConversion('double')->width(2340);
      $this->addMediaConversion('double-webp')->width(2340)->format('webp');
      $this->addMediaConversion('thumbnail')->crop('crop-center', 400, 267);
      $this->addMediaConversion('featured')->crop('crop-center', 800, 534);
      $this->addMediaConversion('featured-webp')->crop('crop-center', 800, 534)->format('webp');
  }

  public function registerMediaCollections(): void
{
    $this->addMediaCollection('landingpages')->singleFile();
    $this->addMediaCollection('landingpages1')->singleFile();
    $this->addMediaCollection('landingpages2')->singleFile();
  }
  
  public function pictures(){
    return $this->hasMany('App\Models\Picture');
  }

  public function projects(){
    return $this->belongsToMany('App\Models\Project', 'landing_page_project')->withPivot('project_id');
  }
}