<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Post extends Model implements HasMedia
{

    use HasFactory;
    use InteractsWithMedia;

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($post) {
            $post->slug = Str::slug($post->title, "-");
        });
    }

    protected $casts = [
        'created_at' => 'datetime',
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->width(1170);
        $this->addMediaConversion('normal-webp')->width(1170)->format('webp');
        $this->addMediaConversion('double')->keepOriginalImageFormat()->width(2340);
        $this->addMediaConversion('double-webp')->width(2340)->format('webp');
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop('crop-center', 400, 267);
        $this->addMediaConversion('featured')->keepOriginalImageFormat()->crop('crop-center', 800, 534);
        $this->addMediaConversion('featured-webp')->crop('crop-center', 800, 534)->format('webp');
    }
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('blogs')->singleFile();
    }

    public function categories(){
        return $this->belongsToMany('App\Models\Category', 'category_post')->withPivot('category_id');
    }

}