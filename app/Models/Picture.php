<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Picture extends Model implements HasMedia
{
  use HasFactory;
  use InteractsWithMedia;

 	protected $fillable = [
	  'size','photo','order','video_link','project_id'
	];

	public function registerMediaConversions(Media $media = null): void
  {
  	$this->addMediaConversion('normal')->width(1200);
    $this->addMediaConversion('normal-webp')->width(1200)->format('webp');
    $this->addMediaConversion('thumbnail')->crop('crop-center', 400, 400);
  }

  public function registerMediaCollections(): void
{
        $this->addMediaCollection('pictures')->singleFile();
    }

  public function project(){
      return $this->belongsTo('App\Models\Project');
  }
}
