<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Category;
use App\Post;
use App\Image;

class PostController extends Controller
{

    // Return news as an array
    public function getPosts(Request $request, $category, $limit){
        $search = $request->input('search');
        if($category == 'all'){
            $posts = Post::where('title','LIKE','%'.$search.'%')
            ->orderBy('created_at','desc')
            ->with('category')
            ->paginate($limit);
        }else{
            $posts = Post::orderBy('created_at','desc')->whereHas('category', function($q) use($category){
                $q->where('slug', $category);
            })
            ->where('title','LIKE','%'.$search.'%')
            ->orderBy('created_at','desc')
            ->with('category')
            ->paginate($limit);
        }
        foreach($posts as $p){
            $p->image = $p->getFirstMediaUrl('featured');
        }
        return $posts;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.blog.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('relates_to', 'posts')->get();
        return view('admin.blog.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|string|max:255',
            'exerpt' => 'required',
            'status' => 'required',
            'category' => 'required',
            'body' => 'required',
            'image' => 'required',
        ]);
        if ($request->hasFile('image') && $request->file('image')->isValid() && $request->file('image')->getClientOriginalName() != "") {
            $article = Post::create([
                'title' => $request->input('title'),
                'exerpt'=> $request->input('exerpt'),
                'status'=> $request->input('status'),
                'category_id'=> $request->input('category'),
                'body' => $request->input('body')
            ]);
            $article->addMediaFromRequest('image')->toMediaCollection('featured', 'media');
        }
        return $article;
    }

    // Function to handle uploading images in the body
    public function imageUpload(Request $request)
    {
        $this->validate($request,[
            'img' => 'required'
        ]);
        $img = Image::create([]);
        if ($request->hasFile('img') && $request->file('img')->isValid() && $request->file('img')->getClientOriginalName() != "") {
            $img->addMediaFromRequest('img')->toMediaCollection('blog', 'media');
        }
        $media = $img->getFirstMediaUrl('blog','normal');
        return $media;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $post = Post::where('id',$post->id)->with('category')->first();
        return view('admin.blog.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::where('relates_to', 'posts')->get();
        $post = Post::where('id', $post->id)->with('category')->first();
        return view('admin.blog.edit', compact('post','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $this->validate($request,[
            'title' => 'required|string|max:255',
            'exerpt' => 'required',
            'status' => 'required',
            'category' => 'required',
            'body' => 'required',
        ]);

        if ($request->hasFile('image') && $request->file('image')->isValid() && $request->file('image')->getClientOriginalName() != "") {
            $post->media->each->delete();
            $post->addMediaFromRequest('image')->toMediaCollection('featured', 'media');
        }

        $update = [
            'title' => $request->input('title'),
            'exerpt'=> $request->input('exerpt'),
            'status'=> $request->input('status'),
            'category_id'=> $request->input('category'),
            'body' => $request->input('body')
        ];

        $post->update($update);

        return $post;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->media->each->delete();
        $post->delete();
        return "success";
    }
}
