<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class SendMail extends Controller
{


  public function enquiry(Request $request){

    // Validate the form data
    $this->validate($request,[
      'name' => 'required|string|max:255',
      'email' => 'required|email',
      'message' => 'required'
    ]); 

    $subject = 'General Enquiry';
    $name = $request->input('name');
    $phone = $request->input('phone');
    $company = $request->input('company');
    $email = $request->input('email');
    $content = $request->input('message');

      //Validation was successful, add your form submission logic here
      Mail::send('emails.enquiry',[
        'name' => $name,
        'phone' => $phone,
        'company' => $company,
        'email' => $email,
        'content' => $content,
        'subject' => $subject
      ], function ($message) use ($subject, $email, $company){
        $message->from('donotreply@elementseven.co', 'Element Seven');
        $message->subject($subject);
        $message->replyTo($email);
        $message->to('hello@elementseven.co');
      });
      return response('Message Sent', 200);

    return response()->json(['message' => 'Error - Unprocessable Entity (validation failed)'], 422);
  }

    // Function to send new enquiry message
	// public function enquiry(Request $request){

	// 	// Validate the form data
 //    $this->validate($request,[
 //      'name' => 'required|string|max:255',
 //      'email' => 'required|email',
 //      'message' => 'required',
	// 		'token' => 'required'
 //    ]); 

	// 	$subject = 'General Enquiry';
	// 	$name = $request->input('name');
	// 	$phone = $request->input('phone');
 //    $company = $request->input('company');
	// 	$email = $request->input('email');
	// 	$content = $request->input('message');

	// 	$url = 'https://www.google.com/recaptcha/api/siteverify';
	// 	$remoteip = $_SERVER['REMOTE_ADDR'];
	// 	$data = [
 //      'secret' => '6LcM2tMZAAAAAIDxjma-q22E0m-_d5wVyW5t7_Bp',
 //      'response' => $request->get('token'),
 //      'remoteip' => $remoteip
 //    ];
	// 	$options = [
 //      'http' => [
 //        'header' => "Content-type: application/x-www-form-urlencoded\r\n",
 //        'method' => 'POST',
 //        'content' => http_build_query($data)
 //      ]
 //    ];
	// 	$context = stream_context_create($options);
 //    $result = file_get_contents($url, false, $context);
 //    $resultJson = json_decode($result);
	// 	if($resultJson->success != true) {
 //      return response()->json($resultJson, 422);
 //    }
	// 	if($resultJson->score >= 0.1) {
 //      //Validation was successful, add your form submission logic here
 //      Mail::send('emails.enquiry',[
 //        'name' => $name,
 //        'phone' => $phone,
 //        'company' => $company,
 //        'email' => $email,
 //        'content' => $content,
 //        'subject' => $subject
 //      ], function ($message) use ($subject, $email, $company){
 //        $message->from('donotreply@elementseven.co', 'Element Seven');
 //        $message->subject($subject);
 //        $message->replyTo($email);
 //        $message->to('hello@elementseven.co');
 //      });
 //      return response('Message Sent', 200);
	// 	}else {
 //      return response()->json(['message' => 'Score too low '.$resultJson->score.' ReCaptcha Error - Unprocessable Entity (validation failed)'], 422);
	// 	}
	// 	return response()->json(['message' => 'ReCaptcha Error - Unprocessable Entity (validation failed)'], 422);
 //  }
}
