<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Project;
use App\Models\Category;
use App\Models\LandingPage;
use Carbon\Carbon;

class PageController extends Controller
{
    // Homepage
    public function welcome(){
        $posts = Post::where('status','!=','draft')->orderBy('created_at','desc')->take(2)->get();
        foreach($posts as $p){
            $p->featured = $p->getFirstMediaUrl('featured');
            $p->featuredwebp = $p->getFirstMediaUrl('featured-webp');
        }
        return view('welcome', compact('posts'));
    }

    // Services
    public function services(){
        return view('services.index');
    }

    // Blog
    public function blog(){
        $categories = Category::has('posts')->orderBy('name','asc')->get();
        return view('blog.index')->with(['categories' => $categories]);
    }

    // Blog post
    public function blogShow($date, $slug){
        if($date == "page"){
            return redirect()->to('/404');
        }
        $date = Carbon::createFromFormat('Y-m-d', $date)->toDateString();
        $post = Post::whereDate('created_at',$date)->where('slug',$slug)->first();
        $post->others = Post::where('id','!=', $post->id)->where('status', 'published')->orderBy('created_at','desc')->paginate(2);
        foreach($post->others as $p){
            $p->image = $p->getFirstMediaUrl('blogs','featured');
            $p->webp = $p->getFirstMediaUrl('blogs','featured-webp');
            $p->mime = $p->getFirstMedia('blogs')->mime_type;
        }

        $post->image = $post->getFirstMediaUrl('blogs','double');
        $post->webp = $post->getFirstMediaUrl('blogs','double-webp');
        $post->mime = $post->getFirstMedia('blogs')->mime_type;
        return view('blog.show')->with(['post' => $post]);
    }

    public function getPosts(Request $request){
        $category =  $request->input('category');
        $search = $request->input('search');
        $limit = $request->input('limit');
        if($category == '*' || $category == '0' || $category == 0){
            $posts = Post::where('title','LIKE','%'.$search.'%')
            ->where('status', '!=', 'draft')
            ->orderBy('created_at','desc')
            ->with('categories')
            ->paginate($limit,['id','title', 'slug', 'excerpt','created_at']);
        }else{
            $posts = Post::orderBy('created_at','desc')->whereHas('categories', function($q) use($category){
                $q->where('id', $category);
            })
            ->where('status', '!=', 'draft')
            ->where('title','LIKE','%'.$search.'%')
            ->orderBy('created_at','desc')
            ->with('categories')
            ->paginate($limit,['id','title', 'slug', 'excerpt','created_at']);
        }
        foreach($posts as $p){
            $p->image = $p->getFirstMediaUrl('blogs','featured');
            $p->webp = $p->getFirstMediaUrl('blogs','featured-webp');
            $p->mime = $p->getFirstMedia('blogs')->mime_type;

        }
        return $posts;
    }

    public function getCategories(){
        $categories = Category::has('posts')->orderBy('name','asc')->get();
        return $categories;
    }

    // Work
    public function work(){
        $categories = Category::has('projects')->orderBy('name','asc')->get();
        return view('work.index')->with(['categories' => $categories]);
    }

    public function workShow(Request $request, $slug){
        $project = Project::where('slug',$slug)->with('category')->with(['pictures' => function ($query) {
            $query->orderBy('order', 'asc');
        }])->first();
        $project->others = Project::where('id','!=', $project->id)->where('status', 'published')->with('category','pictures')->orderBy('created_at','desc')->paginate(2);
        foreach($project->pictures as $p){
            if(!$p->video_link){
                $p->image = $p->getFirstMediaUrl('pictures','normal');
                $p->webp = $p->getFirstMediaUrl('pictures','normal-webp');
                $p->mime = $p->getFirstMedia('pictures')->mime_type;
            }
        }
        foreach($project->others as $p){
            $p->image = $p->getFirstMediaUrl('projects','featured');
            $p->webp = $p->getFirstMediaUrl('projects','featured-webp');
            $p->mime = $p->getFirstMedia('projects')->mime_type;
        }
        $project->image = $project->getFirstMediaUrl('projects','double');
        $project->webp = $project->getFirstMediaUrl('projects','double-webp');
        $project->mime = $project->getFirstMedia('projects')->mime_type;

        return view('work.show')->with(['project' => $project]);

    }

    // Project
    public function showWork($slug){
        $project = Project::where('slug',$slug)->with('category')->with(['pictures' => function ($query) {
            $query->orderBy('order', 'asc');
        }])->first();
        $project->others = Project::where('id','!=', $project->id)->where('status', 'published')->with('category','pictures')->orderBy('created_at','desc')->paginate(2);
        foreach($project->pictures as $p){
            if(!$p->video_link){
                $p->image = $p->getFirstMediaUrl('pictures','normal');
                $p->webp = $p->getFirstMediaUrl('pictures','normal-webp');
                $p->mime = $p->getFirstMedia('pictures')->mime_type;
            }
        }
        foreach($project->others as $p){
            $p->image = $p->getFirstMediaUrl('projects','featured');
            $p->webp = $p->getFirstMediaUrl('projects','featured-webp');
            $p->mime = $p->getFirstMedia('projects')->mime_type;
        }
        $project->image = $project->getFirstMediaUrl('projects','double');
        $project->webp = $project->getFirstMediaUrl('projects','double-webp');
        $project->mime = $project->getFirstMedia('projects')->mime_type;
        return $project;
    }

    public function landingPage($slug){
        $landingpage = LandingPage::where('slug',$slug)->with('projects.category')->first();

        foreach($landingpage->projects as $p){
            $p->image = $p->getFirstMediaUrl('projects','featured');
            $p->webp = $p->getFirstMediaUrl('projects','featured-webp');
            $p->mime = $p->getFirstMedia('projects')->mime_type;
        }
        $landingpage->image = $landingpage->getFirstMediaUrl('landingpages','double');
        $landingpage->webp = $landingpage->getFirstMediaUrl('landingpages','double-webp');
        $landingpage->mime = $landingpage->getFirstMedia('landingpages')->mime_type;

        $landingpage->landingpage1 = $landingpage->getFirstMediaUrl('landingpages1','double');
        $landingpage->landingpage1webp = $landingpage->getFirstMediaUrl('landingpages1','double-webp');
        $landingpage->landingpage1mime = $landingpage->getFirstMedia('landingpages1')->mime_type;

        $landingpage->landingpage2 = $landingpage->getFirstMediaUrl('landingpages2','double');
        $landingpage->landingpage2webp = $landingpage->getFirstMediaUrl('landingpages2','double-webp');
        $landingpage->landingpage2mime = $landingpage->getFirstMedia('landingpages2')->mime_type;

        return view('landingpages.show')->with(['landingpage' => $landingpage]);
    }
    public function getProjects(Request $request){
        $category =  $request->input('category');
        $limit =  $request->input('limit');
        $search = $request->input('search');
        if($category == '*'){
            $projects = Project::orderBy('created_at','desc')
            ->where('status', '!=', 'draft')
            ->with('category')
            ->paginate($limit,['id','title', 'slug', 'excerpt','created_at','category_id']);
        }else{
            $projects = Project::whereHas('category', function($q) use($category){
                $q->where('id', $category);
            })
            ->where('status', '!=', 'draft')
            ->orderBy('created_at','desc')
            ->with('category')
            ->paginate($limit,['id','title', 'slug', 'excerpt','created_at','category_id']);
        }
        foreach($projects as $p){
            $p->image = $p->getFirstMediaUrl('projects','featured');
            $p->webp = $p->getFirstMediaUrl('projects','featured-webp');
            $p->mime = $p->getFirstMedia('projects')->mime_type;
        }
        return $projects;
    }

	public function content(){
        return view('info.content');
    }
    public function compterms(){
        return view('terms.compterms');
    }
    public function voucher(){
        return view('terms.voucher');
    }
    public function tandcs(){
        return view('terms.tandcs');
    }
    public function tandcsv1(){
        return view('terms.version1');
    }

    public function servicesWebsites(){
        $projects = Project::whereHas('category', function($q){
            $q->where('id', 8);
        })
        ->orderBy('created_at','desc')
        ->with('category')
        ->paginate(2,['id','title', 'slug', 'excerpt','created_at','category_id']);
        foreach($projects as $p){
            $p->image = $p->getFirstMediaUrl('projects','featured');
            $p->webp = $p->getFirstMediaUrl('projects','featured-webp');
            $p->mime = $p->getFirstMedia('projects')->mime_type;
        }
    	return view('services.websites')->with(['projects' => $projects]);
    }
    public function servicesSeo(){
        $posts = Post::whereHas('categories', function($q){
            $q->where('id', 5);
        })
        ->orderBy('created_at','desc')
        ->where('status','!=','draft')
        ->paginate(2,['id','title', 'slug', 'excerpt','created_at']);
        foreach($posts as $p){
            $p->image = $p->getFirstMediaUrl('blogs','featured');
            $p->webp = $p->getFirstMediaUrl('blogs','featured-webp');
            $p->mime = $p->getFirstMedia('blogs')->mime_type;
        }
        return view('services.seo')->with(['posts' => $posts]);
    }
    public function servicesEcommerce(){
        $projects = Project::whereHas('category', function($q){
            $q->where('id', 8);
        })
        ->orderBy('created_at','desc')
        ->with('category')
        ->paginate(2,['id','title', 'slug', 'excerpt','created_at','category_id']);
        foreach($projects as $p){
            $p->image = $p->getFirstMediaUrl('projects','featured');
            $p->webp = $p->getFirstMediaUrl('projects','featured-webp');
            $p->mime = $p->getFirstMedia('projects')->mime_type;
        }
        return view('services.ecommerce')->with(['projects' => $projects]);
    }
    public function servicesSocialmedia(){
        $posts = Post::whereHas('categories', function($q){
            $q->where('id', 4);
        })
        ->orderBy('created_at','desc')
        ->where('status','!=','draft')
        ->paginate(2,['id','title', 'slug', 'excerpt','created_at']);
        foreach($posts as $p){
            $p->image = $p->getFirstMediaUrl('blogs','featured');
            $p->webp = $p->getFirstMediaUrl('blogs','featured-webp');
            $p->mime = $p->getFirstMedia('blogs')->mime_type;
        }
        return view('services.socialmedia')->with(['posts' => $posts]);
    }
    public function servicesAftercare(){
        $projects = Project::whereHas('category', function($q){
            $q->where('id', 8);
        })
        ->orderBy('created_at','desc')
        ->with('category')
        ->paginate(2,['id','title', 'slug', 'excerpt','created_at','category_id']);
        foreach($projects as $p){
            $p->image = $p->getFirstMediaUrl('projects','featured');
            $p->webp = $p->getFirstMediaUrl('projects','featured-webp');
            $p->mime = $p->getFirstMedia('projects')->mime_type;
        }
        return view('services.aftercare')->with(['projects' => $projects]);
    }
    public function contact(){
        return view('contact');
    }

}
