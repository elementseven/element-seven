<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Trix;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class LandingPage extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\LandingPage::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array('published'=>'Published','draft'=>'Draft');
        return [
            Select::make('Status')->options($statuses)->sortable(),
            Text::make('Fancy')->onlyOnForms(),
            Text::make('Title')->sortable(),
            Text::make('Excerpt')->sortable()->onlyOnForms(),
            Image::make('Photo')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('photo')->toMediaCollection('landingpages');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('landingpages', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('landingpages', 'thumbnail');
            })->deletable(false),
            Text::make('Fancy1','fancy_1')->onlyOnForms(),
            Text::make('Title1','title_1')->onlyOnForms(),
            Trix::make('Text1','text_1')->onlyOnForms(),
            Image::make('Photo1','photo_1')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('photo_1')->toMediaCollection('landingpages1');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('landingpages1', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('landingpages1', 'thumbnail');
            })->deletable(false),
            Text::make('Testimonial')->onlyOnForms(),
            Text::make('Testimonial Name','testimonial_name')->onlyOnForms(),
            Text::make('Testimonial Project','testimonial_project')->onlyOnForms(),
            Text::make('Testimonial Routerlink','testimonial_routerlink')->onlyOnForms(),
            Text::make('Fancy2','fancy_2')->onlyOnForms(),
            Text::make('Title2','title_2')->onlyOnForms(),
            Trix::make('Text2','text_2')->onlyOnForms(),
            Image::make('Photo2','photo_2')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('photo_2')->toMediaCollection('landingpages2');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('landingpages2', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('landingpages2', 'thumbnail');
            })->deletable(false),
            
            
            Trix::make('Body')->onlyOnForms(),
            BelongsToMany::make('Projects'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
