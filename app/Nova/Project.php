<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\DateTime;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class Project extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Project::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array('published'=>'Published','draft'=>'Draft');
        return [
            Text::make('Title')->sortable(),
            DateTime::make('Created At', 'created_at')->sortable(),
            Image::make('Photo')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('photo')->toMediaCollection('projects');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('projects', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('projects', 'thumbnail');
            })->deletable(false),
            Select::make('Status')->options($statuses)->sortable(),
            BelongsTo::make('Category'),
            Text::make('Link')->sortable()->onlyOnForms(),
            Text::make('Excerpt')->sortable()->onlyOnForms(),
            Trix::make('Body'),
            HasMany::make('Pictures'),
            BelongsToMany::make('LandingPages'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
