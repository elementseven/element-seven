<template>
	<div class="container mob-px-3 pt-3 position-relative">
    <div class="blue-dots-top-right d-none d-lg-block" data-aos="slide-left"></div>
    <div class="pink-pentagon-top-right" data-aos="slide-up" data-aos-delay="200"></div>
		<div class="row mob-py-0" v-if="projects != null">
      <div class="col-lg-6 mb-5" v-for="(project, index) in projects">
        <a :href="'/work/' + project.slug">
          <div class="zoom-link" data-aos="fade-in">
            <div class="blog-img position-relative">
              <picture>
                <source :srcset="project.webp" type="image/webp"/>
                <source :srcset="project.image" :type="project.mime"/> 
                <img :src="project.image" :type="project.mime" :alt="project.title" class="w-100" />
              </picture>
              <div class="pointer"><span></span></div>
            </div>
            <p class="text-primary mb-0 mt-3 letter-spacing text-fancy text-large position-relative">{{project.category.name}}</p>
            <h5 class="text-white">{{project.title}}</h5>
            <p class="text-white text-small">{{project.excerpt}}</p>
          </div>
        </a>
      </div>
		</div>
	</div>
</template>
<script>
	  export default {
  	props:{
  		category: Number
  	},
    data() {
      return {
        limit: 2,
        errors: {},
        success: false,
        loaded: false,
        projects:[],
        page: 1,
        cat: '*',
        search: '',
        api: '/fetch/work/get',
      }
    },
    mounted(){
      this.getProjects();
    },

    methods: {
      getProjects() {
        axios.get(this.api, {
          params: {
            page: this.page,
            category: this.category,
            limit: this.limit
          },
        }).then(({ data }) => {
          this.projects.push(...data.data);
          this.loaded = true;
          console.log(this.projects);
        }).catch(error => {
          // Error handling
          this.loaded = true;
          console.log(error);
        });
      },
      backToTop(){
        setTimeout(function(){
          document.body.scrollTop = document.documentElement.scrollTop = 0;
        },250);
      },
      getExtension(d){
        if (d > 3 && d < 21) return 'th';
        switch (d % 10) {
          case 1:  return "st";
          case 2:  return "nd";
          case 3:  return "rd";
          default: return "th";
        }
      },
      niceDate: function (date) {
        var newdate = new Date(date);
        var thedate = newdate.getDate();
        var extension = this.getExtension(thedate);
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return thedate + extension + ' ' + months[newdate.getMonth()] + ' ' + newdate.getFullYear();
        // return moment(date).format('Do MMM YYYY');
      },
      moment: function (date) {
        var newdate = new Date(date);
        var thedate = newdate.getDate();
        var extension = this.getExtension(thedate);
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return newdate.getFullYear() + '-' + (newdate.getMonth() + 1).toString().padStart(2, '0') + '-' + thedate.toString().padStart(2, '0');
        // return moment(date).format('Do MMM YYYY');
      },
      square(){
        $(".square").each(function(){
          var theWidth = $(this).width();
          $(this).height(theWidth + "px");
        });
      },
      slug: function (article) {
        var slug = article.slug;
        return slug;
      }
    }
  }
</script>
<style lang="scss">
	.zoom-link{
		cursor: pointer;
		.blog-img{
			width: 100%;
			height: 360px;
			max-height: 360px;
			overflow: hidden;
			position: relative;
			img{
				position: absolute;
		    top:-100%; left:0; right: 0; bottom:-100%;
		    margin: auto;
		    @include transition(300ms);
			}
		}
		&:hover{
			.blog-img{
				img{
					transform: scale(1.1);
				}
			}
		}
	}
	.blue-dots-top-right{
    background-image: url('/img/shapes/dots_blue.svg');
    background-size: 100%;
    background-repeat: no-repeat;
    position: absolute;
    top: -60px;
    right: -200px;
    width: 250px;
    height: 418px;
    transform: rotate(-110deg) !important;
    -webkit-transform: rotate(-110deg) !important;
  }
  .pink-pentagon-top-right{
    background-image: url('/img/shapes/pentagon_pink.svg');
    background-size: 100%;
    background-repeat: no-repeat;
    position: absolute;
    top: -100px;
    right: calc(-150px + 1rem);
    width: 300px;
    height: 300px;
    -webkit-animation: rotating 100s linear infinite;
    -moz-animation: rotating 100s linear infinite;
    -ms-animation: rotating 100s linear infinite;
    -o-animation: rotating 100s linear infinite;
    animation: rotating 100s linear infinite;
   }
   @keyframes rotating {
    from {
      -ms-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -webkit-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    to {
      -ms-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -webkit-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
   .pointer {
    position: absolute;
    bottom: 1rem;
    right: 1rem;
    display: block;
    width: 10px;
    height: 10px;
    border-radius: 50%;
    background: $pink;
    cursor: pointer;
    box-shadow: 0 0 0 rgb(255,50,162);
    animation: pulse 2s infinite;
  }
  .pointer span {
    color: #fff;
    text-align: center;
    display: block;
    width: 31px;
    padding: 5px 0px;
  }
  @-webkit-keyframes pulse {
    0% {
      -webkit-box-shadow: 0 0 0 0 rgba(255,50,162, 0.4);
    }
    70% {
        -webkit-box-shadow: 0 0 0 10px rgba(255,50,162, 0);
    }
    100% {
        -webkit-box-shadow: 0 0 0 0 rgba(255,50,162, 0);
    }
  }
  @keyframes pulse {
    0% {
      -moz-box-shadow: 0 0 0 0 rgba(255,50,162, 0.4);
      box-shadow: 0 0 0 0 rgba(255,50,162, 0.4);
    }
    70% {
        -moz-box-shadow: 0 0 0 10px rgba(255,50,162, 0);
        box-shadow: 0 0 0 10px rgba(255,50,162, 0);
    }
    100% {
        -moz-box-shadow: 0 0 0 0 rgba(255,50,162, 0);
        box-shadow: 0 0 0 0 rgba(255,50,162, 0);
    }
  }
  @media only screen and (max-width : 767px){
    .pink-pentagon-top-right{
      width: 250px;
      height: 250px;
    }
  }
</style>