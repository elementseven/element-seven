/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
 require("babel-polyfill");
 require("whatwg-fetch");
 require('./bootstrap');

 import Vue from 'vue';

 require('./plugins/modernizr-custom.js');
 require('./plugins/cookieConsent.js');
 require('waypoints/lib/jquery.waypoints.min.js');
 require('./a-general.js');

 import VueLazyload from "vue-lazyload";
 Vue.use(VueLazyload, {
  lazyComponent: true
});

 window.AOS = require('AOS');
 AOS.init();
 window.addEventListener('load', AOS.refresh);
 window.addEventListener('resize', AOS.refresh);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('InfiniteLoading',  () => import(/* webpackChunkName: "InfiniteLoading" */'vue-infinite-loading'));
Vue.component('cool-button', () => import(/* webpackChunkName: "CoolButton" */'./components/Button.vue') );

Vue.component('welcome-header', () => import(/* webpackChunkName: "WelcomeHeader" */'./components/Welcome/Header.vue') );
Vue.component('welcome-main', () => import(/* webpackChunkName: "WelcomeMain" */'./components/Welcome/Main.vue') );

Vue.component('main-menu', () => import(/* webpackChunkName: "MainMenu" */'./components/Menus/MainMenu.vue') );
Vue.component('scroll-menu', () => import(/* webpackChunkName: "MainMenu" */'./components/Menus/ScrollMenu.vue') );
Vue.component('mobile-menu', () => import(/* webpackChunkName: "MobileMenu" */'./components/Menus/MobileMenu.vue') );
Vue.component('site-footer', () => import(/* webpackChunkName: "SiteFooter" */'./components/SiteFooter.vue') );
Vue.component('footer-top', () => import(/* webpackChunkName: "FooterTop" */'./components/FooterTop.vue') );
Vue.component('loader', () => import(/* webpackChunkName: "Loader" */'./components/Loader.vue') );

Vue.component('work-index', () => import(/* webpackChunkName: "WorkIndex" */'./components/Work/Index.vue') );
Vue.component('work-inline', () => import(/* webpackChunkName: "WorkInline" */'./components/Work/Inline.vue') );

Vue.component('blog-index', () => import(/* webpackChunkName: "BlogIndex" */'./components/Blog/Index.vue') );
Vue.component('blog-inline', () => import(/* webpackChunkName: "BlogInline" */'./components/Blog/Inline.vue') );

Vue.component('contact-page-form', () => import(/* webpackChunkName: "ContactPageForm" */'./components/ContactPageForm.vue') );

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 const app = new Vue({
  el: '#app',
});