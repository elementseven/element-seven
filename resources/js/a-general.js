require('./plugins/modernizr-custom.js');
var windowWidth = $(window).width();
var windowHeight = $(window).height();

// Listen for menu button click
function menuBtn(){
	$('#menu-trigger').each(function(index, element){
		var inview = new Waypoint({
			element: element,
			handler: function (direction) {
				if(direction === "down"){
					$('#scroll-menu').addClass('on');
				}else{
					$('#scroll-menu').removeClass('on');
				}
			},offset: '0%'
		});
	});
}

function lineDrawing(){
	$('.scroll-line').each(function(index, element){
		var effect = $(this).attr("data-line");
		var inview = new Waypoint({
			element: element,
			handler: function (direction) {
				if (direction === 'down') {
					$(element).addClass(effect);
				}
			},offset: '80%'
		});
	});
}

// Function to resize elements to full height
function fullHeight(){
	// Get the current height and width of the window
	windowWidth = $(window).width();
	windowHeight = $(window).height();
	$(".full-Height").each(function(){
		$(this).height(windowHeight + "px");
	});
}

$(document).ready(function(){
	$('#close-loader-btn').click(function(){
		$('#loader').removeClass('on');
		$('#loader-message').html('');
		$('#close-loader-btn').removeClass('d-block').addClass('d-none');
		$('#loader-roller').removeClass('d-none').addClass('d-block');
		$('#loader-success').removeClass('d-block').addClass('d-none');
	});
	$('#menu_btn').click(function(){
		$("#menu_btn .nav-icon").toggleClass('nav_open');
		$("#mobile-menu").toggleClass("on");
		$("#menu").toggleClass("on");
		$("#menu_body_hide").toggleClass("on");
		$("#content").toggleClass("opacity");
		$(".menu").each(function(){
			$(this).toggleClass("opacity");
		});
	});
	$('#close-loader-btn').click(function(){
		document.getElementById("loader").classList.remove("on");
	});

	fullHeight();
	lineDrawing();
	menuBtn();
	AOS.init();
});

$(window).on('resize', function(){
	// Resize full height elements when the window size changes
	fullHeight();
	lineDrawing();
	menuBtn();
});