@php
$page = 'Work';
$pagetitle = $project->title . " - Our Work - Element Seven Belfast NI";
$metadescription = $project->excerpt;
$pagetype = 'dark';
$pagename = 'home';
$ogimage = $project->image;
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative py-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 py-5 mob-pt-0">
    	<p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative z-2"><a href="/work"><i class="fa fa-link mr-2 cursor-pointer"></i><b class="text-pink cursor-pointer">Check out more of our work</b></a></p>
      <h1 class="mob-mt-0 faq-title">{{$project->title}}</h1>
      <p class="text-primary mb-4 letter-spacing text-fancy text-large position-relative">{{$project->category->name}}</p>
      <p class="statement mb-4 scroll-line" data-line="draw-line">{{$project->excerpt}}...</p>
      <cool-button link="{{$project->link}}" :linktype="'_blank'" :color="'#fff'" :text="'Visit Site'" data-aos="fade-up"></cool-button>
    </div>
    <div class="col-lg-12 text-center">
      <div class="rounded-image mob-mb-3">
        <div class="dots-and-circle-right"><div class="dots-beyond"></div> <div class="circle-beyond"></div></div>
        <picture>
          <source srcset="{{$project->image}}" type="{{$project->mime}}"/> 
          <source srcset="{{$project->webp}}" type="image/webp"/>
          <img src="{{$project->image}}" type="{{$project->mime}}" alt="{{$project->title}} ' featured image'" class="w-100" />
        </picture>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="page">
	<div class="container">
	  <div class="row pb-5 mob-py-0">
	    <div class="col-lg-12 mob-mt-0 blog-body">
	      <div class="project-body mb-5">{!!$project->body!!}</div>
	      @foreach($project->pictures as $pic)
	      @if($pic->video_link == null)
	      <div class="col-lg-{{$pic->size}} mb-5">
	      	<picture>
	          <source srcset="{{$pic->image}}" type="{{$project->mime}}"/> 
	          <source srcset="{{$pic->webp}}" type="image/webp"/>
	          <img src="{{$pic->image}}" type="{{$project->mime}}" alt="{{$project->title}} featured image" class="w-100" />
	        </picture>
		    </div>
		    @else
		    <div class="col-lg-{{$pic->size}} mb-5">
			    {!!$pic->video_link!!}
			  </div>
		    @endif
		    @endforeach
	      <div class="fb-like mt-5" data-href="/work/{{$project->slug}}" data-width="" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
	      <div class="d-block mt-3">
	      	<cool-button link="/work/{{$project->slug}}" :linktype="'_blank'" :color="'#fff'" :text="'Visit Site'" data-aos="fade-up"></cool-button>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="container-fluid py-5 position-relative">
	  <div class="dots-line-bottom-right"></div>
	  <div class="row">
	    <div class="container pt-5">
	      <div class="row">
	        <div class="col-12 mb-4 mob-mb-0">
	          <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative"><a href="/work"><i class="fa fa-link mr-2 cursor-pointer"></i><b class="text-pink cursor-pointer">Check out more of our work</b></a></p>
	          <h3 class="mb-4 text-white mb-4">More Work</h3>
	        </div>
	      </div>
	      <div class="row position-relative">
					<div class="blue-dots-top-right d-none d-lg-block" data-aos="slide-left"></div>
	    		<div class="pink-pentagon-top-right d-none d-lg-block" data-aos="slide-up" data-aos-delay="200"></div>
	    		@foreach($project->others as $other)
	        <div class="col-lg-6 mb-5">
	        	<a href="/work/{{$other->slug}}">
	            <div class="zoom-link" data-aos="fade-in">
	              <div class="blog-img position-relative">
	              	<picture>
			              <source srcset="{{$other->image}}" type="{{$other->mime}}"/> 
			              <source srcset="{{$other->webp}}" type="image/webp"/>
			              <img src="{{$other->image}}" type="{{$other->mime}}" alt="{{$other->title}}" class="w-100" />
			            </picture>
			            <div class="pointer"><span></span></div>
	              </div>
	              <p class="text-primary mt-3 mb-0 letter-spacing text-fancy text-large position-relative">{{$other->category->name}}</p>
	              <h5 class="text-white">{{$other->title}}</h5>
	              <p class="text-white text-small">{{$other->excerpt}}</p>
	            </div>
	          </a>
	        </div>
	        @endforeach
	      </div>
	    </div>
	  </div>
	</div>
</div>
@endsection
@section('scripts')
@endsection