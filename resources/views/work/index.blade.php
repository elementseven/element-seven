@php
$page = 'Work';
$pagetitle = "Our Work - Websites, Web Apps, Social Media & More - Element Seven | Belfast, Northern Ireland";
$metadescription = "View some of our recent projects and see why using the latest web design & development techniques works for our clients.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://elementseven.co/img/og-work.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 pt-5 mob-pt-0">
    	<p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative"><b class="text-pink cursor-pointer">Browse our recent projects</b></p>
      <h1 class="mob-mt-0 faq-title">Our Work</h1>
    </div>
  </div>
</header>
@endsection
@section('content')
<work-index :categories="{{$categories}}" :limit="4"></work-index>
@endsection
@section('scripts')
@endsection