@php
$page = 'Work';
$pagetitle = "Our Work - Web Designer blog Belfast, Northern Ireland | Element Seven";
$metadescription = "View some of our recent projects and see why using the latest web design & development techniques works for our clients.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = $post->image;
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative py-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 pt-5 mob-pt-0">
    	<p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative z-2"><a href="/blog"><i class="fa fa-link mr-2 cursor-pointer"></i><b class="text-pink cursor-pointer">Browse our blog posts</b></a></p>
      <h1 class="mob-mt-0 faq-title">{{$post->title}}</h1>
      <p class="text-primary mb-4 letter-spacing text-fancy text-large position-relative">{{Carbon\Carbon::parse($post->created_at)->format('d M Y')}}</p>
      <p class="statement mb-5 scroll-line" data-line="draw-line">{{$post->excerpt}}...</p>
    </div>
    <div class="col-lg-12 text-center">
      <div class="rounded-image mob-mb-3">
        <div class="dots-and-circle-right"><div class="dots-beyond"></div> <div class="circle-beyond"></div></div>
        <picture>
          <source srcset="{{$post->image}}" type="{{$post->mime}}"/> 
          <source srcset="{{$post->webp}}" type="image/webp"/>
          <img src="{{$post->image}}" type="{{$post->mime}}" alt="{{$post->title}} featured image" class="w-100" />
        </picture>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="page">
	<div class="container">
	  <div class="row pb-5 mob-py-0">
	    <div class="col-xl-10 mob-mt-0 blog-body">
	      <div>{!!$post->body!!}</div>
	      <div class="fb-like mt-5" data-href="/blog/{{Carbon\Carbon::parse($post->created_at)->format('Y-m-d')}}/{{$post->slug}}" data-width="" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
	      <p class="text-primary text-small text-uppercase letter-spacing mt-3"><a href="/blog"><i class="fa fa-angle-left" aria-hidden="true"></i> <b>ALL BLOG POSTS</b></a></p>
	    </div>
	  </div>
	</div>
	<div class="container-fluid py-5 position-relative">
	  <div class="dots-line-bottom-right"></div>
	  <div class="row">
	    <div class="container py-5">
	      <div class="row">
	        <div class="col-12 mb-4 mob-mb-0">
	          <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative"><a href="/blog"><i class="fa fa-link mr-2 cursor-pointer"></i><b class="text-pink cursor-pointer">Browse our blog posts</b></a></p>
	          <h3 class="mb-4 text-white mb-4">More Blog Posts</h3>
	        </div>
	      </div>
	      <div class="row position-relative">
	      	<div class="blue-dots-top-right d-none d-lg-block" data-aos="slide-left"></div>
      		<div class="pink-pentagon-top-right d-none d-lg-block" data-aos="slide-up" data-aos-delay="200"></div>
      		@foreach($post->others as $other)
	        <div class="col-lg-6">
	        	<a href="/blog/{{Carbon\Carbon::parse($other->created_at)->format('Y-m-d')}}/{{$other->slug}}">
	            <div class="zoom-link" data-aos="fade-in">
	              <div class="blog-img position-relative">
	              	<picture>
			              <source srcset="{{$other->image}}" type="{{$other->mime}}"/> 
			              <source srcset="{{$other->webp}}" type="image/webp"/>
			              <img src="{{$other->image}}" type="{{$other->mime}}" alt="{{$other->title}}" class="w-100" />
			            </picture>
			            <div class="pointer"><span></span></div>
	              </div>
	              <p class="text-primary mt-3 mb-0 letter-spacing text-fancy text-large position-relative">{{Carbon\Carbon::parse($other->created_at)->format('d M Y')}}</p>
	              <h5 class="text-white">{{$other->title}}</h5>
	              <p class="text-white text-small">{{$other->excerpt}}</p>
	            </div>
	          </a>
	        </div>
	        @endforeach
	      </div>
	    </div>
	  </div>
	</div>
</div>
@endsection
@section('scripts')
@endsection