@php
$page = 'Blog';
$pagetitle = "Blog - Web Design blog Belfast, Northern Ireland | Element Seven";
$metadescription = "Read the Element Seven Blog to find out more about Websites, Web Design and Development in Belfast, Northern Ireland.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://elementseven.co/img/og-blog.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 pt-5 mob-pt-0">
    	<p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative"><b class="text-pink cursor-pointer">Browse our blog posts</b></p>
      <h1 class="mob-mt-0 faq-title">Web Design Blog</h1>
    </div>
  </div>
</header>
@endsection
@section('content')
<blog-index :categories="{{$categories}}" :limit="4"></blog-index>
@endsection
@section('scripts')
@endsection