@php
$page = 'Contact';
$pagetitle = "Contact Element Seven for a Web Design Quote | Phone, Email and Address";
$metadescription = "Get in touch with Element Seven, a professional Web Design and Development company based in Belfast, Northern Ireland.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://elementseven.co/img/og-contact.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5 z-2">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 pt-5 mob-pt-0">
      <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative"><b class="text-pink cursor-pointer">How we can help you</b></p>
      <h1 class="mob-mt-0 page-top mb-5">Let's talk</h1>
      <p class="statement mb-5 scroll-line" data-line="draw-line"><b>Whether its for a quote, a general enquiry or just some friendly advice, we would love to hear from you!</b></p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="page contact-page">
		<div class="container-fluid position-relative">
		  <div class="pink-dots-bottom-left d-none d-lg-block" data-aos="slide-right"></div>
		  <div class="blue-circle-bottom-left" data-aos="slide-up"></div>
      <div class="container mob-px-0">
        <contact-page-form :recaptcha="'6LeOVJkUAAAAAPDPp347RKrh_7B0nsjblaTJn5Kj'"></contact-page-form>
      </div>
	  </div>
    <div class="container py-5 mob-pb-0">
      <div class="row justify-content-center">
        <div class="col-lg-4 pt-5 text-center text-lg-left">
          <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative"><b class="text-pink cursor-pointer">Join us</b></p>
          <h2 class="mb-0">Our People</h2>
          <p class="mb-4">We are a small team, but we are growing! If you want to join us, send us an email!</p>
          <cool-button :link="'mailto:hello@elementseven.co'" :color="'#fff'" :text="'Email Us'"></cool-button>
        </div>
        <div class="col-lg-4 text-center">
          <picture> 
            <source srcset="/img/people/luke.webp" type="image/webp"/> 
            <source srcset="/img/people/luke.png" type="image/png"/>
            <img v-lazy="'/img/people/luke.png'" alt="Luke McKelvey - Technical Director Element Seven Belfast Modern responsive mobile website designer" class="w-100 mb-3 px-4" data-aos="slide-up"/>
          </picture>   
          <p class="mb-1 text-small letter-spacing text-uppercase">Technical Director</p>
          <p class="text-largest text-title text-pink mb-1">Luke McKelvey</p>
          <p class="mb-1"><a class="text-white" href="mailto:luke@elementseven.co">luke@elementseven.co</a></p>
          <p class="mb-2"><a class="text-white" href="tel:00442895031307">028 950 31307</a></p>
          <p>
            <a href="https://www.linkedin.com/in/luke-mckelvey-a0980498" target="_blank"><i class="fa fa-linkedin mr-2"></i></a>
            <a href="https://twitter.com/thelukemckelvey" target="_blank"><i class="fa fa-twitter mr-2"></i></a>
            <a href="https://www.youtube.com/channel/UCL5vJ-iFATWkVbE0jJStbPQ" target="_blank"><i class="fa fa-youtube-play"></i></a>
          </p>
        </div>
        <div class="col-lg-4 text-center">
          <picture> 
            <source srcset="/img/people/kyle.webp" type="image/webp"/> 
            <source srcset="/img/people/kyle.png" type="image/png"/>
            <img v-lazy="'/img/people/kyle.png'" alt="Kyle Ferguson - Creative Director Element Seven Belfast Modern responsive mobile website designer" class="w-100 mb-3 px-4" data-aos="slide-up" data-aos-delay="200"/>
          </picture>   
          <p class="mb-1 text-small letter-spacing text-uppercase">Creative Director</p>
          <p class="text-largest text-title text-pink mb-1">Kyle Ferguson</p>
          <p class="mb-1"><a class="text-white" href="mailto:kyle@elementseven.co">kyle@elementseven.co</a></p>
          <p class="mb-2"><a class="text-white" href="tel:00442895031307">028 950 31307</a></p>
          <p>
            <a href="https://www.linkedin.com/in/kyle-ferguson-6944b1151/" target="_blank"><i class="fa fa-linkedin mr-2"></i></a>
            <a href="https://twitter.com/KyleFerguson92" target="_blank"><i class="fa fa-twitter mr-2"></i></a>
            <a href="https://www.youtube.com/channel/UCL5vJ-iFATWkVbE0jJStbPQ" target="_blank"><i class="fa fa-youtube-play"></i></a>
          </p>
        </div>
      </div>
    </div>
		<div class="bg-primary-light py-5 mt-5">
    	<div class="container py-5 position-relative">
        <div class="row">
          <div class="col-lg-3 text-center text-lg-left">
            <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink cursor-pointer">Come say Hi!</b></p>
            <h3>Find us</h3>
            <p class="mb-3">136 Lisburn Road,<br/>Belfast, BT1 2LB</p>
            <p class="mb-1"><a href="tel:02895031307" class="text-primary">028 950 31307</a></p>
            <p class="mb-1"><a href="mailto:hello@elementseven.co" class="text-primary">hello@elementseven.co</a></p>
            <p class="mt-3"><a href="https://www.facebook.com/elementsevendigital/" class="mr-3" target="_blank">
                <i class="fa fa-facebook text-pink"></i>
              </a>
              <a href="https://www.instagram.com/elementsevendigital/" class="mr-3" target="_blank">
                <i class="fa fa-instagram text-pink"></i>
              </a>
              <a href="https://twitter.com/elementsevenco" class="mr-3" target="_blank">
                <i class="fa fa-twitter text-pink"></i>
              </a>
              <a href="https://www.linkedin.com/company/element-seven/" target="_blank"><i class="fa fa-linkedin text-pink"></i>
              </a>
             </p>
          </div>
          <div class="col-lg-9 position-relative">
          	<div class="pink-pentagon-top-right" data-aos="slide-up" data-aos-delay="200"></div>
            <div id="map"></div>
          </div>
        </div>
	    </div>
		</div>
		<loader></loader>
	</div>
@endsection
@section('scripts')
<script>
	window.initMap = function () {
		var marker;
  	var map;
    var mapstyle = [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"color":"#000000"},{"lightness":13}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#144b53"},{"lightness":14},{"weight":1.4}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#08304b"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#0c4152"},{"lightness":5}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#09ffc3"},{"lightness":25}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#ff09d8"},{"lightness":16}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"}]},{"featureType":"transit","elementType":"all","stylers":[{"color":"#146474"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#021019"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]}];
    var mapDiv = document.getElementById('map');
    var ellatlng = {lat: 54.583218764469976, lng: -5.944762725814788};
    var myIcon = {
      url: "/img/icons/map_pin2.png",
      size: new window.google.maps.Size(80, 132),
      origin: new window.google.maps.Point(0, 0),
      anchor: new window.google.maps.Point(40, 90),
      scaledSize: new window.google.maps.Size(80, 132)
    };
    map = new window.google.maps.Map(mapDiv, {
      center: ellatlng,
      scrollwheel: false,
      mapTypeControl: false,
      draggable: false,
      streetViewControl: false,
      fullscreenControl: false,
      zoom: 17
    });
    marker = new window.google.maps.Marker({
        position: ellatlng,
        map: map,
        animation: window.google.maps.Animation.DROP,
        flat: true,
        icon: myIcon,
        draggable: false,
        title: "Element Seven",
        'optimized': false
    });
    map.set('styles', mapstyle);
	}

</script>
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJwAp_2r4ZyeXAMax2GXqao1RkEU_1TI8&region=GB&language=en-gb&callback=initMap"></script>
@endsection