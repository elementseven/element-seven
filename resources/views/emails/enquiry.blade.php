<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      
	        <table width="500px" style="font-family:'Tahoma', 'Geneva', sans-serif;, serif; text-align: left; font-weight:100" align="center">
				
				<tr style="margin:40px 0 40px 0"><td>

				<p style="font-size:45px; color: #6844b4; margin: 0; text-align: left;">Element Seven</p>

				<p style="font-size:24px; border-bottom: 2px solid #6844b4; padding-bottom: 30px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Tahoma', 'Geneva', sans-serif;">General Enquiry</span></p>

				<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Tahoma', 'Geneva', sans-serif;"><b>Name:</b></span> {{$name}}</p>

				<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Tahoma', 'Geneva', sans-serif;"><b>Email:</b></span> {{$email}}</p>

				@if($phone)
				<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Tahoma', 'Geneva', sans-serif;"><b>Phone:</b></span> {{$phone}}</p>
				@endif
				@if($company)
				<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Tahoma', 'Geneva', sans-serif;"><b>Company:</b></span> {{$company}}</p>
				@endif
				<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Tahoma', 'Geneva', sans-serif;"><b>Message:</b></span><br><br> {{$content}}</p>

				</td>
				</tr>
				<tr><td><br>
				<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif;">This is an email sent from the enquiries form on the Element Seven website.<br><a href="https://www.elementseven.co">www.elementseven.co</a></p></td></tr>

			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>