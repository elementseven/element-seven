<!DOCTYPE html>
<html id="wav-html" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="canonical" href="https://haymarketbelfast.com/">
  <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="/img/favicon/site.webmanifest">
  <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#111111">
  <link rel="shortcut icon" href="/img/favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#111111">
  <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <title>{{$pagetitle}}</title>
  <meta property="og:type" content="website">
  <meta property="og:title" content="{{$pagetitle}}">
  <meta property="og:url" content="{{Request::url()}}">
  <meta property="og:site_name" content="Element Seven">
  <meta property="og:locale" content="en_GB">
  <meta property="og:image" content="{{$ogimage}}">
  <meta property="og:image:secure" content="{{$ogimage}}">
  <meta property="og:image:type" content="image/jpeg">
  <meta property="og:image:width" content="1200" /> 
  <meta property="og:image:height" content="627" />
  <meta property="og:updated_time" content="2021-04-14T21:23:54+0000" />
  <meta property="og:description" content="{{$metadescription}}">
  <meta property="fb:app_id" content="627343197767633" />
  <meta name="google-site-verification" content="VsfIc-eqP6pslKMJO4R3dVAjvY8avwGvJTTiIrDhdQM" />
  <meta name="description" content="{{$metadescription}}">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link rel="preload" as="font" href="/fonts/fontawesome-webfont.woff2?v=4.7.0" type="font/woff2" crossorigin="anonymous">
  <link rel="prefetch" href="https://elementseven.co/img/logos/logo-full-grad.svg" />
  <link href="{{ mix('css/app.min.css') }}" rel="stylesheet"/>
  @yield('styles')
  <script type="application/ld+json">
   {
    "@context" : "https://schema.org",
    "@type" : "Organization",       
    "telephone": "+442895031307",
    "contactType": "Customer service"
  }
</script>
<script>window.dataLayer = window.dataLayer || [];</script>
@yield('head_section')
</head>  
<body class="front">
<div id="fb-root"></div>
<div id="main-wrapper">
  <div id="app" class="front {{$pagetype}} overflow-hidden mw-100" data-scroll-container>
    <main-menu></main-menu>
    <scroll-menu></scroll-menu>
    <mobile-menu></mobile-menu>
    @yield('header')
    <main id="content" style="z-index: 2;" class="">
      <div id="menu-trigger"></div>
      @yield('content')
    </main>
    <lazy-component>
      <footer-top></footer-top>
    </lazy-component>
    <site-footer></site-footer>
    @yield('modals')
    <loader></loader>
  </div>
  <div id="menu_body_hide"></div>
</div>
@yield('prescripts')
<script src="{{ mix('/js/manifest.js') }}"></script>
<script src="{{ mix('/js/vendor.js') }}"></script>
<script src="{{ mix('js/app.js') }}"></script>
@yield('scripts')
<script>
  window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
	    "popup": {
	      "background": "#090919",
	      "text": "#ffffff"
	    },
	    "button": {
	      "background": "#ff32a2",
	      "text": "#ffffff"
	    }
	  }
	})});
 </script>

      <!-- Google Tag Manager -->
{{--   <script async defer>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-KT24V5H');</script> --}}
  <!-- End Google Tag Manager -->
<!-- Google Tag Manager (noscript) -->
  {{-- <noscript><iframe name="Tag Manager" title="Google Tag Manager" src="https://www.googletagmanager.com/ns.html?id=GTM-KT24V5H"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> --}}
  <!-- End Google Tag Manager (noscript) -->
</body>
</html>