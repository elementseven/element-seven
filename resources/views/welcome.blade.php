@php
$page = 'Homepage';
$pagetitle = "Element Seven - Bespoke Web Design Belfast, Northern Ireland";
$metadescription = "Bespoke Web Design company in Belfast, Northern Ireland. We work with a range of Established Businesses & Start-Ups, specialising in Web Design, SEO & Digital Marketing.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://elementseven.co/img/og-home.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<welcome-header></welcome-header>
@endsection
@section('content')
<welcome-main></welcome-main>
@endsection
@section('scripts')
@endsection