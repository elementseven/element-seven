@php
$page = 'Services';
$pagetitle = "Services - Websites, SEO, E-commerce, Social Media & Aftercare | Element Seven";
$metadescription = "Learn about the range of services available to our clients in Belfast, Northern Ireland. Everything from web design to social media campaigns, if it's online, we can help you.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://elementseven.co/img/og-services.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 pt-5 mob-pt-0">
      <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative"><b class="text-pink cursor-pointer">How we can help you</b></p>
      <h1 class="mob-mt-0 page-top mb-5">Digital Services</h1>
      <p class="statement mb-5 scroll-line" data-line="draw-line"><b>Our business relies on producing great results for our clients online so they sing our praises to their customers, friends and colleagues. We help ourselves by helping you!</b></p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="page">
	<div class="container-fluid bg-dark text-white">
	  <div class="row">
	    <div class="container wide-container">
	      <div class="row justify-content-center ipad-py-5">
	      	<div class="col-12 d-lg-none">
            <picture> 
              <source srcset="/img/services/websites.webp" type="image/webp"/> 
              <source srcset="/img/services/websites.jpg" type="image/jpeg"/>
              <img src="/img/services/websites.jpg" alt="Websites by element seven are respinsive, affordable and modern" class="w-100"  data-aos="fade-in"/>
            </picture>   
	        </div>
	        <div class="col-lg-5 offset-lg-1">
	          <div class="d-table w-100 h-100">
	            <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left">
	              <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink cursor-pointer">Modern & Responsive</b></p>
	              <h3 class="mb-4" data-aos="fade-in">Websites</h3>
	              <p class="mb-4" data-aos="fade-in">Professional web design helps to make your business appear credible online. It is the process of conceptualising, planning, and building a collection of electronic files that deliver pages to your site visitors. </p>
	              <cool-button :link="'/services/websites'" :color="'#fff'" :text="'Tell me more'" data-aos="fade-up"></cool-button>
	            </div>
	          </div>
	        </div>
	        <div class="col-lg-6 d-none d-lg-block">
            <picture> 
              <source srcset="/img/services/websites.webp" type="image/webp"/> 
              <source srcset="/img/services/websites.jpg" type="image/jpeg"/>
              <img src="/img/services/websites.jpg" alt="Websites by element seven are respinsive, affordable and modern" class="w-100"  data-aos="fade-in"/>
            </picture>   
	        </div>
	      </div>
	      <div class="row mob-mt-5 ipad-py-5">
	        <div class="col-lg-6">
	          <picture> 
              <source srcset="/img/services/ecommerce.webp" type="image/webp"/> 
              <source srcset="/img/services/ecommerce.jpg" type="image/jpeg"/>
              <img src="/img/services/ecommerce.jpg" alt="Websites by element seven are respinsive, affordable and modern" class="w-100"  data-aos="fade-in"/>
            </picture>   
	        </div>
	        <div class="col-lg-5">
	          <div class="d-table w-100 h-100">
	            <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left">
	              <p class="mb-0"></p>
	              <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink cursor-pointer">Smart & Reliable</b></p>
	              <h3 class="mb-4" data-aos="fade-in">E-commerce</h3>
	              <p class="mb-4">E-commerce is the practice of selling goods or services online through a payment facility on your website. An e-commerce website opens up the potential of the internet to your business, allowing you to compliment your traditional sales with online revenue. </p>
	              <cool-button :link="'/services/e-commerce'" :color="'#fff'" :text="'Learn More'" data-aos="fade-up"></cool-button>
	            </div>
	          </div>
	        </div>
	      </div>
        <div class="row justify-content-center mob-mt-5 ipad-py-5">
        	<div class="col-12 d-lg-none">
            <picture> 
              <source srcset="/img/services/socialmedia.webp" type="image/webp"/> 
              <source srcset="/img/services/socialmedia.jpg" type="image/jpeg"/>
              <img src="/img/services/socialmedia.jpg" alt="Websites by element seven are respinsive, affordable and modern" class="w-100"  data-aos="fade-in"/>
            </picture>   
	        </div>
          <div class="col-lg-5 offset-lg-1">
            <div class="d-table w-100 h-100">
              <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left">
                <p class="mb-0"></p>
                <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink cursor-pointer">Impressive & Effective</b></p>
                <h3 class="mb-4" data-aos="fade-in">Social Media Marketing</h3>
                <p class="mb-4">When beginning a social media campaign there are a lot of factors to consider. When we identify who we are trying to put content in front of, we can begin to design the campaign that will trigger a click from a potential customer.</p>
                <cool-button :link="'/services/social-media'" :color="'#fff'" :text="'Learn More'" data-aos="fade-up"></cool-button>
              </div>
            </div>
          </div>
          <div class="col-lg-6 d-none d-lg-block">
            <picture> 
              <source srcset="/img/services/socialmedia.webp" type="image/webp"/> 
              <source srcset="/img/services/socialmedia.jpg" type="image/jpeg"/>
              <img src="/img/services/socialmedia.jpg" alt="Websites by element seven are respinsive, affordable and modern" class="w-100"  data-aos="fade-in"/>
            </picture>   
          </div>
        </div>
	      <div class="row mob-mt-5 ipad-py-5">
	        <div class="col-lg-6">
	          <picture> 
              <source srcset="/img/services/aftercare.webp" type="image/webp"/> 
              <source srcset="/img/services/aftercare.jpg" type="image/jpeg"/>
              <img src="/img/services/aftercare.jpg" alt="Websites by element seven are respinsive, affordable and modern" class="w-100"  data-aos="fade-in"/>
            </picture>   
	        </div>
	        <div class="col-lg-5">
	          <div class="d-table w-100 h-100">
	            <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left">
	              <p class="mb-0"></p>
	              <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink cursor-pointer">Sensible & Important</b></p>
	              <h3 class="mb-4" data-aos="fade-in">Aftercare</h3>
	              <p class="mb-4">Once your awesome new website is launched, we don’t want to close the door behind you and never see you again. There is so much you can do after launch to improve your search engine ranking, website traffic and conversions.</p>
	              <cool-button :link="'/services/aftercare'" :color="'#fff'" :text="'Learn More'" data-aos="fade-up"></cool-button>
	            </div>
	          </div>
	        </div>
	      </div>
        <div class="row justify-content-center pb-5 mb-5 mob-mt-5 ipad-py-5">
        	<div class="col-12 d-lg-none">
            <picture> 
              <source srcset="/img/services/seo.webp" type="image/webp"/> 
              <source srcset="/img/services/seo.jpg" type="image/jpeg"/>
              <img src="/img/services/seo.jpg" alt="Websites by element seven are respinsive, affordable and modern" class="w-100"  data-aos="fade-in"/>
            </picture>   
	        </div>
          <div class="col-lg-5 offset-lg-1">
            <div class="d-table w-100 h-100">
              <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left">
                <p class="mb-0"></p>
                <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink cursor-pointer">Effective & Accountable</b></p>
                <h3 class="mb-4" data-aos="fade-in">Search Engine Optimisation</h3>
                <p class="mb-4">Google ranks websites they “crawl” on the internet based on a range of different factors. It is important that all these are addressed and optimised before a website is launched and also as an ongoing process to ensure positive results.</p>
                <cool-button :link="'/services/seo'" :color="'#fff'" :text="'Learn More'" data-aos="fade-up"></cool-button>
              </div>
            </div>
          </div>
          <div class="col-lg-6 d-none d-lg-block">
            <picture> 
              <source srcset="/img/services/seo.webp" type="image/webp"/> 
              <source srcset="/img/services/seo.jpg" type="image/jpeg"/>
              <img src="/img/services/seo.jpg" alt="Websites by element seven are respinsive, affordable and modern" class="w-100"  data-aos="fade-in"/>
            </picture>   
          </div>
        </div>
	    </div>
	  </div>
	</div>
</div>
@endsection
@section('scripts')
@endsection