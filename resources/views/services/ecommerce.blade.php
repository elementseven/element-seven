@php
$page = 'E-commerce';
$pagetitle = "E-commerce - Sell online through your website with Element Seven";
$metadescription = "Element seven, Northern Ireland's Most Exciting Agency in 2019. We provide excellent eCommerce websites for our clients, in Belfast. Specialising in Shopify, Aimeos and WooCommerce.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://elementseven.co/img/og-ecommerce.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 pt-5 mob-pt-0">
      <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative"><b class="text-pink cursor-pointer">Conversion Focused</b></p>
      <h1 class="mob-mt-0 page-top mb-5">E-commerce</h1>
      <p class="statement scroll-line" data-line="draw-line"><b>Sell your products and services online safely with one of our custom built, E-commerce websites. Keep track of your sales and conversions using the latest in tracking technology.</b></p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="page services-page">
  <div class="container wide-container py-5">
    <div class="row mob-pt-0">
      <div class="col-lg-6 mob-pt-0">
        <picture> 
          <source srcset="/img/services/ecommerce.webp" type="image/webp"/> 
          <source srcset="/img/services/ecommerce.jpg" type="image/jpeg"/>
          <img src="/img/services/ecommerce.jpg" alt="Websites by element seven are respinsive, affordable and modern" class="w-100"  data-aos="fade-in"/>
        </picture>      
      </div>
      <div class="col-lg-6">
        <div class="d-table w-100 h-100">
          <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left">
            <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink">The burning question</b></p>
            <h2 class="mb-4 smaller" data-aos="fade-in">What is E-commerce?</h2>
            <p class="mb-4 text-large">E-commerce is the practice of selling goods or services online through a payment facility on your website. This might involve selling a single product or a catalogue of items.</p>
            <cool-button :link="'/contact'" :color="'#fff'" :text="'Contact Us'" data-aos="fade-up"></cool-button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid position-relative">
    <div class="pink-dots-bottom-left d-none d-lg-block" data-aos="slide-right"></div>
    <div class="blue-circle-bottom-left d-none d-lg-block" data-aos="slide-up"></div>
    <div class="row">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-10 text-center py-5 mb-5 ">
            <p class="text-large text-pink"><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="100"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="200"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="300"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="400"></i><i class="fa fa-star" data-aos="fade-in" data-aos-delay="500"></i></p>
            <p class="mimic-h2 section-title mb-4 smaller" data-aos="fade-in">What our clients say...</p>
            
            <p class="mb-3 pb-2 position-relative" data-aos="fade-in"><i>"Element seven recently developed our new website. The service we received was exceptional from start to finish. Work was completed quickly and we are delighted with our site!  Thanks so much!"</i></p>
            <p class="mb-4"><b>Paul Rowland - <a href="/work/a-plus-motoring-academy">A Plus Motoring Academy</a></b></p>
            <cool-button :link="'/contact'" :color="'#ffffff'" :text="'Get in touch'" data-aos="fade-up"></cool-button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container wide-container">
    <div class="row justify-content-center position-relative">
      <div class="col-lg-5 offset-lg-1 pt-5 pb-3 mob-py-0 py-5 text-center text-lg-left">
        <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink">We Deliver</b></p>
        <h3 class="mimic-h2 mb-4 position-relative">Online Sales</h3>
        <p class="mb-4 pb-2 text-large position-relative" data-aos="fade-in">An e-commerce website opens up the potential of the internet to your business, allowing you to compliment your traditional sales with online revenue. There are many different methods of building e-commerce websites, each with their own advantages and disadvantages. You may have heard of technologies like Shopify, Magento, Wordpress and Amieos, all of which are viable methods for selling online.</p>
        <cool-button :link="'/contact'" :color="'#fff'" :text="'Contact Us'" data-aos="fade-up"></cool-button>
      </div>
      <div class="col-lg-6 py-5 mob-pb-0">
        <picture> 
          <source srcset="/img/services/ecommerce2.webp" type="image/webp"/> 
          <source srcset="/img/services/ecommerce2.jpg" type="image/jpeg"/>
          <img src="/img/services/ecommerce2.jpg" alt="Responsive websites belfast northern ireland" class="w-100" data-aos="slide-up"/>
        </picture>   
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row mt-5 mob-mt-0">
      <div class="col-lg-12">
        <div class="py-5">
          <p class="statement scroll-line" data-line="draw-line">We love building e-commerce websites as they offer fantastic metrics, that show direct return on investment against a new website. If you are interested in learning more about making money online, just <a href="/contact">get in touch!</a></p>
        </div>
      </div>
    </div>
  </div>
  <div class="container position-relative">
    <div class="row py-5 mob-py-0 mt-5 mob=mt-0">
      <div class="col-12">
        <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative z-2" data-aos="fade-in"><a href="/work"><i class="fa fa-link mr-2 cursor-pointer"></i><b class="text-pink cursor-pointer">Check out our work</b></a></p>
        <p class="mimic-h3 mb-4 text-white mob-mb-2" data-aos="fade-in">Recent Websites</p>
      </div> 
      <work-inline :category="8"></work-inline>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection