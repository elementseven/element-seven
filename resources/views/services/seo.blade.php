@php
$page = 'SEO';
$pagetitle = "SEO - Grow Your Business & Drive Website Traffic with Element Seven";
$metadescription = "Element Seven is a SEO Focused Web Agency Belfast, Northern Ireland. Grow Your Business, Drive Sales & Improve Brand Awareness with Increased Traffic through Search Engine Optimisation.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://elementseven.co/img/og-seo.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 pt-5 mob-pt-0">
      <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative"><b class="text-pink">Quality Traffic</b></p>
      <h1 class="mob-mt-0 page-top mb-5">Search Engine Optimisation</h1>
      <p class="statement scroll-line" data-line="draw-line"><b>One of the most important factors when creating or updating a website is SEO. If no one visits your website it is useless to you, no matter how much you spend on it or pretty it is.</b></p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="page services-page">
  <div class="container wide-container py-5 mob-pt-0">
    <div class="row mob-pt-0">
      <div class="col-lg-6">
        <picture> 
          <source srcset="/img/services/seo.webp" type="image/webp"/> 
          <source srcset="/img/services/seo.jpg" type="image/jpeg"/>
          <img src="/img/services/seo.jpg" alt="Websites by element seven are respinsive, affordable and modern" class="w-100"  data-aos="fade-in"/>
        </picture>      
      </div>
      <div class="col-lg-6">
        <div class="d-table w-100 h-100">
          <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left">
            <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink">What's it all about?</b></p>
            <h2 class="mb-4 smaller" data-aos="fade-in">What is SEO?</h2>
            <p class="mb-4 text-large">SEO stands for Search Engine Optimization, which is the practice of increasing the quantity and quality of traffic to your website through organic search engine results.</p>
            <cool-button :link="'/contact'" :color="'#ffffff'" :text="'Get in touch'" data-aos="fade-up"></cool-button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid position-relative">
    <div class="pink-dots-bottom-left d-none d-lg-block" data-aos="slide-right"></div>
    <div class="blue-circle-bottom-left d-none d-lg-block" data-aos="slide-up"></div>
    <div class="row">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-10 text-center py-5 mb-5 ">
            <p class="text-large text-pink"><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="100"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="200"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="300"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="400"></i><i class="fa fa-star" data-aos="fade-in" data-aos-delay="500"></i></p>
            <p class="mimic-h2 section-title mb-4 smaller" data-aos="fade-in">What our clients say...</p>
            
            <p class="mb-3 pb-2 position-relative" data-aos="fade-in"><i>"We find it essential to find the right partners in Business.  Element Seven are that, and we look forward to developing our relationship more in the future. The site produced was perfect for our needs and they were always helpful in identifying where we can excel further in our online services.  Top class all round from the team at Element Seven."</i></p>
            <p class="mb-4"><b>Conor Owens - <a href="/work/belfast-hidden-tours">Belfast Hidden Tours</a></b></p>
            <cool-button :link="'/contact'" :color="'#ffffff'" :text="'Get in touch'" data-aos="fade-up"></cool-button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container wide-container">
    <div class="row justify-content-center position-relative">
      <div class="col-lg-5 offset-lg-1 pt-5 pb-3 mob-py-0 py-5 text-center text-lg-left">
        <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink">We can help</b></p>
        <h3 class="mimic-h2 mb-4 position-relative">Ahead of the curve</h3> 
        <p class="mb-4 pb-2 text-large position-relative" data-aos="fade-in">Our SEO packages offer a monthly evaluation of your website, it’s traffic and performance in search results for the keywords associated with your business online. We test and tweak each of the factors mentioned in the paragraph above to make sure your website is going up the rankings rather than down.</p>
        <cool-button :link="'/contact'" :color="'#ffffff'" :text="'Get in touch'" data-aos="fade-up"></cool-button>
      </div>
      <div class="col-lg-6 pl-5 mob-px-3">
        <picture> 
          <source srcset="/img/services/seo2.webp" type="image/webp"/> 
          <source srcset="/img/services/seo2.jpg" type="image/jpeg"/>
          <img src="/img/services/seo2.jpg" alt="Responsive websites belfast northern ireland" class="w-100" data-aos="slide-up"/>
        </picture>   
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row mt-5 mob-mt-0">
      <div class="col-lg-12">
        <div class="py-5">
          <p class="statement scroll-line" data-line="draw-line">Google ranks webpages they “crawl” on the internet based on a range of different factors like the quality of your content, page load speeds, user experience and accessibility issues for disabled users. All of these factors should be optimised before a website is launched and also as an ongoing process to improve results.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid bg-dark ">
    <div class="row pt-5 mob-pt-0">
      <div class="container position-relative">
        <div class="row pt-5">
          <div class="col-12">
            <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><a href="/blog"><i class="fa fa-link mr-2 cursor-pointer"></i><b class="text-pink cursor-pointer">Browse our blog posts</b></a></p>
            <h3 class="mb-4 text-white" data-aos="fade-in">SEO Blogs</h3>
          </div> 
          <blog-inline :category="5"></blog-inline>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection