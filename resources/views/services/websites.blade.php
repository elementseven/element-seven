@php
$page = 'Websites';
$pagetitle = "Websites | Element Seven - Web Design Agency in Belfast, Northern Ireland";
$metadescription = "Web design is what we do. Specialising in custom, responsive websites, Your new website will improve your digital presence, drives sales and boost brand awareness.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://elementseven.co/img/og-websites.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 pt-5 mob-pt-0">
      <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative"><b class="text-pink cursor-pointer">Modern & Responsive</b></p>
      <h1 class="mob-mt-0 page-top mb-5">Websites</h1>
      <p class="statement scroll-line" data-line="draw-line"><b>Modern, responsive websites, built using the best techniques. Your new website will improve your digital presence, drives sales and boost brand awareness.</b></p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="page services-page">
  <div class="container wide-container py-5 mob-pt-0">
    <div class="row mob-pt-0">
      <div class="col-lg-6">
        <picture> 
          <source srcset="/img/services/websites.webp" type="image/webp"/> 
          <source srcset="/img/services/websites.jpg" type="image/jpeg"/>
          <img src="/img/services/websites.jpg" alt="Websites by element seven are respinsive, affordable and modern" class="w-100"  data-aos="fade-in"/>
        </picture>      
      </div>
      <div class="col-lg-6">
        <div class="d-table w-100 h-100">
          <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left">
            <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink">The burning question</b></p>
            <h2 class="mb-4 smaller" data-aos="fade-in">What is web design?</h2>
            <p class="mb-4 text-large">Professional web design improves your business's credibility online. It is the process of determining how a website will look and function, defining the layout, colours, fonts, graphics, images and use of interactive features that deliver a great experience to your customers. </p>
            <cool-button :link='"/contact"' :color="'#fff'" :text="'Contact Us'" data-aos="fade-up"></cool-button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid position-relative">
    <div class="pink-dots-bottom-left d-none d-lg-block" data-aos="slide-right"></div>
    <div class="blue-circle-bottom-left d-none d-lg-block" data-aos="slide-up"></div>
    <div class="row">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-10 text-center py-5 mb-5 ">
            <p class="text-large text-pink"><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="100"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="200"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="300"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="400"></i><i class="fa fa-star" data-aos="fade-in" data-aos-delay="500"></i></p>
            <p class="mimic-h2 section-title mb-4 smaller" data-aos="fade-in">What our clients say...</p>
            
            <p class="mb-3 pb-2 position-relative" data-aos="fade-in"><i>"We find it essential to find the right partners in Business.  Element Seven are that, and we look forward to developing our relationship more in the future. The site produced was perfect for our needs and they were always helpful in identifying where we can excel further in our online services.  Top class all round from the team at Element Seven."</i></p>
            <p class="mb-4"><b>Conor Owens - <a href='/work/belfast-hidden-tours'>Belfast Hidden Tours</a></b></p>
            <cool-button :link='"/contact"' :color="'#ffffff'" :text="'Get in touch'" data-aos="fade-up"></cool-button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container wide-container">
    <div class="row justify-content-center position-relative">
      <div class="col-lg-5 offset-lg-1 pt-5 pb-3 mob-py-0 py-5 text-center text-lg-left">
        <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink">Local Quality</b></p>
        <h3 class="mimic-h2 mb-4 position-relative">Websites,<br/>Built in Belfast</h3>
        <p class="mb-4 pb-2 text-large position-relative" data-aos="fade-in">When designing a website, we always use the latest techniques & the most up to date technology to deliver the best quality website possible. It is important to us that our work stands out from the crowd. There are many web designers in Belfast that will build you something simple and send you on your way, we always strive to go further so your customers are delighted with our work.</p>
        <cool-button :link='"/contact"' :color="'#fff'" :text="'Contact Us'" data-aos="fade-up"></cool-button>
      </div>
      <div class="col-lg-6 pl-5 mob-px-3 py-5 mob-pb-0">
        <picture> 
          <source srcset="/img/services/websites2.webp" type="image/webp"/> 
          <source srcset="/img/services/websites2.jpg" type="image/jpeg"/>
          <img src="/img/services/websites2.jpg" alt="Responsive websites belfast northern ireland" class="w-100" data-aos="slide-up"/>
        </picture>   
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row mt-5 mob-mt-0">
      <div class="col-lg-12">
        <div class="py-5">
          <p class="statement scroll-line" data-line="draw-line">Are you thinking why pay for a custom website when I can just build my own in Wix, Squarespace etc? Have a look at our page on <a href='/what-we-do/website-builders'>website builders</a> where we give an honest review on these services.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="container position-relative">
    <div class="row py-5 mob-py-0 mt-5 mob=mt-0">
      <div class="col-12">
        <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative z-2" data-aos="fade-in"><a href='/work'><i class="fa fa-link mr-2 cursor-pointer"></i><b class="text-pink cursor-pointer">Check out our work</b></a></p>
        <p class="mimic-h3 mb-4 text-white mob-mb-2" data-aos="fade-in">Recent Websites</p>
      </div> 
      <work-inline :category="8"></work-inline>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection