@php
$page = 'Aftercare';
$pagetitle = "Aftercare - Improve your website over time with Element Seven";
$metadescription = "Once your awesome new website is launched, we don’t want to close the door behind you and never see you again. There is so much you can do after launch to improve your search engine ranking, website traffic and conversions.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://elementseven.co/img/og-aftercare.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 pt-5 mob-pt-0">
      <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative"><b class="text-pink cursor-pointer">Progression Focused</b></p>
      <h1 class="mob-mt-0 page-top mb-5">Aftercare</h1>
      <p class="statement scroll-line" data-line="draw-line"><b>Once your awesome new website is launched, we don’t want to close the door behind you and never see you again. There is so much you can do after launch to improve your search engine ranking, website traffic and conversions.</b></p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="page services-page">
  <div class="container wide-container py-5 mob-pt-0">
    <div class="row mob-pt-0">
      <div class="col-lg-6">
        <picture> 
          <source srcset="/img/services/aftercare.webp" type="image/webp"/> 
          <source srcset="/img/services/aftercare.jpg" type="image/jpeg"/>
          <img src="/img/services/aftercare.jpg" alt="Websites by element seven are respinsive, affordable and modern" class="w-100"  data-aos="fade-in"/>
        </picture>      
      </div>
      <div class="col-lg-6">
        <div class="d-table w-100 h-100">
          <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left">
            <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink">The burning question</b></p>
            <h2 class="mb-4 smaller" data-aos="fade-in">What is Aftercare?</h2>
            <p class="mb-4 text-large">Once your awesome new website is up and running, we don't want to close the door behind you on your way out! The internet is dynamic and ever-changing, if your ambition for your business to stay ahead of the competition, our aftercare packages can help you do that.</p>
            <cool-button :link="'/contact'" :color="'#fff'" :text="'Contact Us'" data-aos="fade-up"></cool-button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container position-relative py-5 mob-mt-0 z-2">
    <div class="row mb-5 mob-mb-0">
      <div class="col-12 text-center text-lg-left">
        <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink">What we can do</b></p>
        <p class="mimic-h2 section-title mb-5 smaller" data-aos="fade-in">Service List</p>
      </div>
      <div class="col-lg-4">
        <ul class="text-left pl-4">
          <li class="mb-2"><b>Website audit, delivered via email, checking for hacking &amp; broken features.</b></li>
          <li class="mb-2"><b>Monthly Analytics reports generated and explained via email.</b></li>
          <li class="mb-2"><b>SEO performance analysis.</b></li>
          <li class="mb-2"><b>Implementation of SEO improvements.</b></li>
          <li class="mb-2"><b>Identification of potential negative user experiences.</b></li>
          <li class="mb-2"><b>Basic layout, text and image changes.</b></li>
          <li><b>No Contract.</b></li>
        </ul>
      </div>
      <div class="col-lg-4">
        <ul class="text-left pl-4">
          <li class="mb-2"><b>Advanced Keyword Research &amp; Discovery.</b></li>
          <li class="mb-2"><b>Advanced technical SEO &amp; page load speed Improvements.</b></li>
          <li class="mb-2"><b>Facebook Pixel integration.</b></li>
          <li class="mb-2"><b>Offsite Link Building.</b></li>
          <li class="mb-2"><b>Landing Page Creation.</b></li>
          <li class="mb-2"><b>Continued Content Development.</b></li>
          <li class="mb-2"><b>Advanced layout, text and image changes.</b></li>
          <li><b>No Contract.</b></li>
        </ul>
      </div>
      <div class="col-lg-4">
        <ul class="text-left pl-4">
          <li class="mb-2"><b>Social media graphics.</b></li>
          <li class="mb-2"><b>Targeted social media campaigns.</b></li>
          <li class="mb-2"><b>Competitor analysis.</b></li>
          <li class="mb-2"><b>Mailing list management.</b></li>
          <li class="mb-2"><b><a href="https://mailchimp.com" target="_blank">Mailchimp</a> Email marketing campaigns.</b></li>
          <li class="mb-2"><b>Online marketing strategy and implementation.</b></li>
          <li><b>Additional monthly tasks completed at an agreed rate.</b></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid position-relative">
    <div class="pink-dots-bottom-left d-none d-lg-block" data-aos="slide-right"></div>
    <div class="blue-circle-bottom-left d-none d-lg-block" data-aos="slide-up"></div>
    <div class="row">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-10 text-center py-5 mb-5 ">
            <p class="text-large text-pink"><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="100"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="200"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="300"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="400"></i><i class="fa fa-star" data-aos="fade-in" data-aos-delay="500"></i></p>
            <p class="mimic-h2 section-title mb-4 smaller" data-aos="fade-in">What our clients say...</p>
            
            <p class="mb-3 pb-2 position-relative" data-aos="fade-in"><i>"Element Seven recently redesigned our website and we couldn't be happier.  From first consultation to finished product Element Seven's attention to detail and patience with a litany of daft questions were unfailing.  We at Portadown Tiles & Bathrooms would have no hesitation in recommending Element Seven to anyone."</i></p>
            <p class="mb-4"><b>Jenni Wilson - <a href="/work/portadown-tiles">Portadown Tiles</a></b></p>
            <cool-button :link="'/contact'" :color="'#ffffff'" :text="'Get in touch'" data-aos="fade-up"></cool-button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container wide-container">
    <div class="row justify-content-center position-relative">
      <div class="col-lg-5 offset-lg-1 pt-5 pb-3 mob-py-0 py-5 text-center text-lg-left">
        <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink">Why do it?</b></p>
        <h3 class="mimic-h2 mb-4 position-relative">Do I need Aftercare?</h3>
        <p class="mb-4 pb-2 text-large position-relative" data-aos="fade-in">We will always do everything possible to make sure you get the best results from your initial website launch, however there are certain things you can do over time to your website to improve its performance. All of our websites come with Google Analytics attached to monitor and assess user trends and traffic data. A monthly assessment of this data, can tell us exactly how your users are responding to your new website.</p>
        <cool-button :link="'/contact'" :color="'#fff'" :text="'Contact Us'" data-aos="fade-up"></cool-button>
      </div>
      <div class="col-lg-6 pl-5 mob-px-3 mob-mt-5">
        <picture> 
          <source srcset="/img/services/aftercare2.webp" type="image/webp"/> 
          <source srcset="/img/services/aftercare2.jpg" type="image/jpeg"/>
          <img src="/img/services/aftercare2.jpg" alt="Responsive websites belfast northern ireland" class="w-100" data-aos="slide-up"/>
        </picture>   
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row mt-5 mob-mt-0">
      <div class="col-lg-12">
        <div class="py-5">
          <p class="statement scroll-line" data-line="draw-line">Another great benefit of our aftercare packages is the improvements we can make to your search engine rankings. Google changes it’s ranking criteria all the time and with our Aftercare SEO options, we can make sure you stay on top of the competition and show up for the search terms relevant to your business.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="container position-relative">
    <div class="row py-5 mob-py-0 mt-5 mob=mt-0">
      <div class="col-12">
        <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative z-2" data-aos="fade-in"><a href="/work"><i class="fa fa-link mr-2 cursor-pointer"></i><b class="text-pink cursor-pointer">Check out our work</b></a></p>
        <p class="mimic-h3 mb-4 text-white mob-mb-2" data-aos="fade-in">Recent Websites</p>
      </div> 
      <work-inline :category="8"></work-inline>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection