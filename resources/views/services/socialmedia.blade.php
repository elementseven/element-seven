@php
$page = 'Social Media Marketing';
$pagetitle = "Social Media Marketing | Grow your following & Drive Sales with Element Seven";
$metadescription = "Our social media campaigns identify your potential customers & what they are looking for. Proven social media marketing strategies that are effective for your business with your target audience on platforms ideally suited to you.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://elementseven.co/img/og-social-media.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 pt-5 mob-pt-0">
      <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative"><b class="text-pink">Impressive & Effective</b></p>
      <h1 class="mob-mt-0 page-top mb-5">Social Media Marketing</h1>
      <p class="statement scroll-line mb-5 mob-mb-0" data-line="draw-line"><b>Are you overwhelmed by the demands of Social Media? We can make sure you get the most out of your social channels. From identifying the right target audience, to creating campaign graphics and videos, we can help.</b></p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="page services-page">
  <div class="container wide-container py-5">
    <div class="row mob-pt-0">
      <div class="col-lg-6 mob-pt-0">
        <picture> 
          <source srcset="/img/services/socialmedia.webp" type="image/webp"/> 
          <source srcset="/img/services/socialmedia.jpg" type="image/jpeg"/>
          <img src="/img/services/socialmedia.jpg" alt="Websites by element seven are respinsive, affordable and modern" class="w-100"  data-aos="fade-in"/>
        </picture>      
      </div>
      <div class="col-lg-6">
        <div class="d-table w-100 h-100">
          <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left">
            <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink">Whats the point?</b></p>
            <h2 class="mb-4 smaller" data-aos="fade-in">Why advertise on Social Media?</h2>
            <p class="mb-4 text-large">Advertising on social media is one of the most effective forms of advertising, with the right approach you can reach people interested in what you are selling on a huge scale.</p>
            <cool-button :link="'/contact'" :color="'#fff'" :text="'Contact Us'" data-aos="fade-up"></cool-button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid position-relative">
    <div class="pink-dots-bottom-left d-none d-lg-block" data-aos="slide-right"></div>
    <div class="blue-circle-bottom-left d-none d-lg-block" data-aos="slide-up"></div>
    <div class="row">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-10 text-center py-5 mb-5 ">
            <p class="text-large text-pink"><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="100"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="200"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="300"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="400"></i><i class="fa fa-star" data-aos="fade-in" data-aos-delay="500"></i></p>
            <p class="mimic-h2 section-title mb-4 smaller" data-aos="fade-in">What our clients say...</p>
            
            <p class="mb-3 pb-2 position-relative" data-aos="fade-in"><i>"Brilliant company with fantastic customer service! Super happy with the social media campaign, better outcome than I could have hoped for!! Thanks guys!"</i></p>
            <p class="mb-4"><b>Sinead Kelly - <a href="https://www.facebook.com/DogTownNI" target="_blank">Dogtown NI</a></b></p>
            <cool-button :link="'/contact'" :color="'#ffffff'" :text="'Get in touch'" data-aos="fade-up"></cool-button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container wide-container">
    <div class="row justify-content-center position-relative">
      <div class="col-lg-5 offset-lg-1 pt-5 pb-3 mob-py-0 py-5 text-center text-lg-left">
        <div class="d-table w-100 h-100">
          <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left">
            <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink">We can help</b></p>
            <h3 class="mimic-h2 mb-4 position-relative">More than just posting</h3>
            <p class="mb-4 pb-2 text-large position-relative" data-aos="fade-in">Social media is now one of the most common methods of advertising, if you aren’t doing it, your competitors are and you will be at a huge disadvantage. With our experience running successful campaigns for businesses ranging from car dealerships to personal trainers, we can help you gain the competitive edge.</p>
            <cool-button :link="'/contact'" :color="'#fff'" :text="'Contact Us'" data-aos="fade-up"></cool-button>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <picture> 
          <source srcset="/img/services/socialmedia2.webp" type="image/webp"/> 
          <source srcset="/img/services/socialmedia2.jpg" type="image/jpeg"/>
          <img src="/img/services/socialmedia2.jpg" alt="Responsive websites belfast northern ireland" class="w-100" data-aos="slide-up"/>
        </picture>   
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row mt-5 mob-mt-0"> 
      <div class="col-lg-12">
        <div class="py-5">
          <p class="statement scroll-line" data-line="draw-line">The traceability of social media campaigns gives us the numbers to prove its success. We can tell you exactly how many people clicked the advert, which platform they came from and whether the click resulted in an enquiry or direct sale. We can then use all this valuable data to identify successful elements of the campaign and improve the conversion rate for the next one!</p>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid bg-dark ">
    <div class="row pt-5 mob-pt-0">
      <div class="container position-relative">
        <div class="row pt-5">
          <div class="col-12">
            <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative z-2" data-aos="fade-in"><a href="/blog" ><i class="fa fa-link mr-2 cursor-pointer"></i><b class="text-pink cursor-pointer">Browse our blog posts</b></a></p>
            <h3 class="mb-4 text-white" data-aos="fade-in">Social Media Blogs</h3>
          </div> 
          <blog-inline :category="4"></blog-inline>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection