@php
$page = 'Terms & Conditions';
$pagetitle = "Terms & Conditions | Element Seven";
$metadescription = "Element Seven is a web design and development company based in Belfast, Northern Ireland that specialises in modern, responsive web applications. Our terms and conditions can be found on this page.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://elementseven.co/img/og-home.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 pt-5 mob-pt-0">
      <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative"><b class="text-pink cursor-pointer">How we operate</b></p>
      <h1 class="mob-mt-0 page-top mb-5">Terms &amp; Conditions</h1>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="page">
  <div class="container pb-5">
    <div class="row py-4">
      <div class="col-md-12 pb-5">
        <p class="mb-4"><b>The following terms and conditions apply to all website development / design services provided by Element Seven Digital Limited to the Client.</b></p>
        <p class="mb-0"><b>1. Acceptance</b></p>
        <p>It is not necessary for any Client to have signed an acceptance of these terms and conditions for them to apply. If a Client accepts a quote then the Client will be deemed to have satisfied themselves as to the terms applying and have accepted these terms and conditions in full.<br>
        Please read these terms and conditions carefully. Any purchase or use of our services implies that you have read and accepted our terms and conditions.</p>
        <p class="mb-0"><b>2. Charges</b></p>
        <p>Charges for services to be provided by Element Seven Digital Limited are defined in the project quotation that the Client receives via e-mail. Quotations are valid for a period of 30 days. Element Seven Digital Limited reserves the right to alter or decline to provide a quotation after expiry of the 30 days.<br>Unless agreed otherwise with the Client, all website design services require an advance payment of a minimum of fifty (50) percent of the project quotation total before the work is supplied to the Client for review. A second charge of twenty five (25) percent is required after the design stage and before the development stage begins, with the remaining twenty five (25) percent of the project quotation total due upon completion of the development and testing stage, this final payment needs to be made prior to optimisation, upload to the server and/or release of any materials. These payment terms are negotiable, if different terms are otherwise agreed, they will be confirmed by email.<br>Payment for services is due by bank transfer or PayPal. Bank details will be made available on invoices. If a client chooses to pay with PayPal, the client assumes the cost of any assoicated fees determined by PayPal for the use of their service.To protect the practices of Element Seven Digital Limited, we hold the Intellectual Property rights to all source design files and code files, if you wish to attain these files a minimum fee of 500% of the project cost will need to be paid e.g. a project costing £2,000 would require a fee of £10,000 to be paid to attain these source files.<br></p>

        <p class="mb-0"><b>3. Client Review</b></p>
        <p>Element Seven Digital Limited will provide the Client with an opportunity to review the appearance and content of the website during the design phase and once the overall website development is completed. At the completion of the project, such materials will be deemed to be accepted and approved unless the Client notifies Element Seven Digital Limited otherwise within ten (10) days of the date the materials are made available to the Client. Element Seven will offer 2 sets of revisions during the design stage (unless agreed otherwise), once signed off we will proceed to the next stage of the project and cannot make further changes to the design.</p>

        <p class="mb-0"><b>4. Turnaround Time and Content Control</b></p>
        <p>Element Seven Digital Limited will install and publicly post or supply the Client's website by the date specified in the project proposal, or at date agreed with Client upon Element Seven Digital Limited receiving initial payment, unless a delay is specifically requested by the Client and agreed by Element Seven Digital Limited.<br>

          In return, the Client agrees to delegate a single individual as a primary contact to aid Element Seven Digital Limited with progressing the commission in a satisfactory and expedient manner.<br>

        During the project, Element Seven Digital Limited will require the Client to provide website content; text, images, movies and sound files. Delays to the project due to the client not providing these materials when they are needed are not the responsiblity of Element Seven Digital Limited.</p>

        <p class="mb-0"><b>5. Failure to provide required website content:</b></p>
        <p>Element Seven Digital Limited is a small business, to remain efficient we must ensure that work we have programmed is carried out at the scheduled time. On occasions we may have to reject offers for other work and enquiries to ensure that your work is completed at the time arranged.<br>

          This is why we ask that you provide all the required information in advance. On any occasion where progress cannot be made with your website because we have not been given the required information in the agreed time frame, and we are delayed as result, we reserve the right to impose a surcharge of up to 25%. If your project involves Search Engine Optimisation we need the text content for your site in advance so that the SEO can be planned and completed efficiently.<br>

          If you agree to provide us with the required information and subsequently fail to do within one week of project commencement we reserve the right to close the project and the balance remaining becomes payable immediately. Simply put, all the above condition says is do not give us the go ahead to start until you are ready to do so.<br>

          NOTE: Text content should be delivered as a Microsoft Word, email (or similar) document with the pages in the supplied document representing the content of the relevant pages on your website. These pages should have the same titles as the agreed website pages. Contact us if you need clarification on this.<br>

        Using our content management system you are able to keep your content up to date your self.</p>

        <p class="mb-0"><b>6. Payment</b></p>
        <p>Invoices will be provided by Element Seven Digital Limited upon completion but before publishing the live website. Invoices are normally sent via email; however, the Client may choose to receive hard copy invoices. Invoices are due upon receipt. Accounts that remain unpaid thirty (30) days after the date of the invoice will be assessed and we will invoke our right to exercise our statutory right to interest under the Late Payment of Commercial Debts (Interest) Act 1998. </p>

        <p class="mb-0"><b>7. Additional Expenses</b></p>
        <p>Client agrees to reimburse Element Seven Digital Limited for any additional expenses necessary for the completion of the work. Examples would be purchase of special fonts, stock photography etc.</p>

        <p class="mb-0"><b>8. Web Browsers</b></p>
        <p>Element Seven Digital Limited makes every effort to ensure websites are designed to be viewed by the majority of visitors. Websites are designed to work with the most popular current browsers (e.g. Firefox, Microsoft Edge, Google Chrome, etc.). Client agrees that Element Seven Digital Limited cannot guarantee correct functionality with all browser software across different operating systems.<br>

        Element Seven Digital Limited cannot accept responsibility for web pages which do not display acceptably in new versions of browsers released after the website have been designed and handed over to the Client. As such, Element Seven Digital Limited reserves the right to quote for any work involved in changing the website design or website code for it to work with updated browser software.</p>

        <p class="mb-0"><b>9. Default</b></p>
        <p>Accounts unpaid thirty (30) days after the date of invoice will be considered in default. If the Client in default maintains any information or files on Element Seven Digital Limited's Web space, Element Seven Digital Limited will, at its discretion, remove all such material from its web space. Element Seven Digital Limited is not responsible for any loss of data incurred due to the removal of the service. Removal of such material does not relieve the Client of the obligation to pay any outstanding charges assessed to the Client's account. Cheques returned for insufficient funds will be assessed a return charge of £25 and the Client's account will immediately be considered to be in default until full payment is received. Clients with accounts in default agree to pay Element Seven Digital Limited reasonable expenses, including legal fees and costs for collection by third-party agencies, incurred by Element Seven Digital Limited in enforcing these Terms and Conditions.</p>

        <p class="mb-0"><b>10. Termination</b></p>
        <p>Termination of services by the Client must be requested in a written notice and will be effective on receipt of such notice. E-mail or telephone requests for termination of services will not be honoured until and unless confirmed in writing. The Client will be invoiced for work completed to the date of first notice of cancellation for payment in full within thirty (30) days.</p>

        <p class="mb-0"><b>11. Indemnity</b></p>
        <p>All Element Seven Digital Limited services may be used for lawful purposes only. You agree to indemnify and hold Element Seven Digital Limited harmless from any claims resulting from your use of our service that damages you or any other party.</p>

        <p class="mb-0"><b>12. Copyright</b></p>
        <p>The Client retains the copyright to data, files and graphic logos provided by the Client, and grants Element Seven Digital Limited the rights to publish and use such material. The Client must obtain permission and rights to use any information or files that are copyrighted by a third party. The Client is further responsible for granting Element Seven Digital Limited permission and rights for use of the same and agrees to indemnify and hold harmless Element Seven Digital Limited from any and all claims resulting from the Client's negligence or inability to obtain proper copyright permissions. A contract for website design and/or placement shall be regarded as a guarantee by the Client to Element Seven Digital Limited that all such permissions and authorities have been obtained. Evidence of permissions and authorities may be requested.</p>

        <p class="mb-0"><b>13. Standard Media Delivery</b></p>
        <p>Unless otherwise specified in the project quotation, this Agreement assumes that any text will be provided by the Client in electronic format (ASCII text files delivered on usb drive or via e-mail or FTP) and that all photographs and other graphics will be provided physically in high quality print suitable for scanning or electronically in .gif, .jpeg, .png or .tiff format. Although every reasonable attempt shall be made by Element Seven Digital Limited to return to the Client any images or printed material provided for use in creation of the Client's website, such return cannot be guaranteed.</p>

        <p class="mb-0"><b>14. Design Credit</b></p>
        <p>A link to Element Seven Digital Limited will appear in either small type or by a small graphic at the bottom of the Client's website. If a graphic is used, it will be designed to fit in with the overall site design. If a client requests that the design credit be removed, a nominal fee of 20% of the total development charges will be applied. When total development charges are less than £5000, a fixed fee of £1000 will be applied. The Client also agrees that the website developed for the Client may be presented in Element Seven Digital Limited's portfolio.</p>

        <p class="mb-0"><b>15. Access Requirements</b></p>
        <p>If the Client's website is to be installed on a third-party server, Element Seven Digital Limited must be granted temporary read/write access to the Client's storage directories which must be accessible via FTP. Depending on the specific nature of the project, other resources might also need to be configured on the server.</p>

        <p class="mb-0"><b>16. Post-Placement Alterations</b></p>
        <p>Element Seven Digital Limited cannot accept responsibility for any alterations caused by a third party occurring to the Client's pages once installed. Such alterations include, but are not limited to additions, modifications or deletions.</p>

        <p class="mb-0"><b>17. Domain Names</b></p>
        <p>Element Seven Digital Limited may purchase domain names on behalf of the Client. Payment and renewal of those domain names is the responsibility of the Client. The loss, cancellation or otherwise of the domain brought about by non or late payment is not the responsibility of Element Seven Digital Limited. The Client should keep a record of the due dates for payment to ensure that payment is received in good time.</p>

        <p class="mb-0"><b>18. General</b></p>
        <p>These Terms and Conditions supersede all previous representations, understandings or agreements. The Client's signature below or payment of an advance fee constitutes agreement to and acceptance of these Terms and Conditions. Payment online is an acceptance of our terms and conditions.</p>

        <p class="mb-0"><b>19. Social Media Management</b></p>
        <p>Social Media Marketing and Management is defined as helping a client to promote their products or services through social media channels. Element Seven will honour the components of your chosen social media package, providing an agreement to a minimum 3 months contract is served and monthly payments are received in advance. In the event that payment is not received on time, we regret that further work will be halted until this is rectified.</p>

        <p class="mb-0"><b>20. Governing Law</b></p>
        <p>This Agreement shall be governed by British Law.</p>

        <p class="mb-0"><b>21. Liability</b></p>
        <p>Element Seven Digital Limited hereby excludes itself, its Employees and or Agents from all and any liability from:</p>
        <ul>
          <li class="list-item">Loss or damage caused by any inaccuracy;</li>
          <li class="list-item">Loss or damage caused by omission;</li>
          <li class="list-item">Loss or damage caused by delay or error, whether the result of negligence or other cause in the production of the web site;</li>
          <li class="list-item">Loss or damage to clients' artwork/photos, supplied for the site. Immaterial whether the loss or damage results from negligence or otherwise.</li>
        </ul>
        <p>The entire liability of Element Seven Digital Limited to the Client in respect of any claim whatsoever or breach of this Agreement, whether or not arising out of negligence, shall be limited to the charges paid for the Services under this Agreement in respect of which the breach has arisen.</p>

        <p class="mb-0"><b>22. Severability</b></p>
        <p>In the event any one or more of the provisions of this Agreement shall be held to be invalid, illegal or unenforceable, the remaining provisions of this Agreement shall be unimpaired and the Agreement shall not be void for this reason alone. Such invalid, illegal or unenforceable provision shall be replaced by a mutually acceptable valid, legal and enforceable provision, which comes closest to the intention of the parties underlying the invalid provision.</p>

        <hr/>

        <p class="mb-0"><b>Previous T&Cs</b></p>
        <p>These Terms and Conditions apply to all projects from 29/10/2019 onwards.  If your project was started before this date, please see our previous <a href="/tandcs-28-10-2019" class="text-primary">Terms and Conditions</a>.</p>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection