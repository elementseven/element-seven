@php
$page = 'Landing Page';
$pagetitle = $landingpage->title . ' - What we do - Element Seven';
$metadescription = $landingpage->excerpt;
$pagetype = 'dark';
$pagename = 'home';
$ogimage = $landingpage->image;
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 pt-5 mob-pt-0">
    	<p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative">{{$landingpage->fancy}}</p>
      <h1 class="mob-mt-0 faq-title">{{$landingpage->title}}</h1>
      <p class="text-primary mb-4 letter-spacing text-fancy text-large position-relative"></p>
      <p class="statement mb-5 scroll-line" data-line="draw-line">{{$landingpage->excerpt}}...</p>
    </div>
    <div class="col-lg-12 text-center">
      <div class="rounded-image mob-mb-3">
        <div class="dots-and-circle-right"><div class="dots-beyond"></div> <div class="circle-beyond"></div></div>
        <picture>
          <source srcset="{{$landingpage->image}}" type="{{$landingpage->mime}}"/> 
          <source srcset="{{$landingpage->webp}}" type="image/webp"/>
          <img src="{{$landingpage->image}}" type="{{$landingpage->mime}}" alt="{{$landingpage->title}} featured image" class="w-100" />
        </picture>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="page">
	<div class="container wide-container py-5 mob-pt-0">
	  <div class="row mob-pt-0">
	    <div class="col-lg-6">
	      <picture> 
	      	<source srcset="{{$landingpage->landingpage1}}" type="{{$landingpage->landingpage1mime}}"/> 
          <source srcset="{{$landingpage->landingpage1webp}}" type="image/webp"/>
          <img src="{{$landingpage->landingpage1}}" type="{{$landingpage->landingpage1mime}}" alt="{{$landingpage->title}} featured image" class="w-100"  data-aos="fade-in"/>
        </picture>      
	    </div>
	    <div class="col-lg-6">
        <div class="d-table w-100 h-100">
          <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left">
			      <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink">{{$landingpage->fancy_1}}</b></p>
			      <h2 class="mb-4 smaller" data-aos="fade-in">{{$landingpage->title_1}}</h2>
			      <div class="mb-4 text-large lp-text">{!!$landingpage->text_1!!}</div>
			      <cool-button :link="'/contact'" :color="'#fff'" :text="'Contact Us'" data-aos="fade-up"></cool-button>
			    </div>
			  </div>
	    </div>
	  </div>
	</div>
	<div class="container-fluid position-relative">
	  <div class="pink-dots-bottom-left d-none d-lg-block" data-aos="slide-right"></div>
	  <div class="blue-circle-bottom-left d-none d-lg-block" data-aos="slide-up"></div>
	  <div class="row">
	    <div class="container">
	      <div class="row justify-content-center">
	        <div class="col-lg-10 text-center py-5 mb-5 ">
	          <p class="text-large text-pink"><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="100"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="200"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="300"></i><i class="fa fa-star mr-1" data-aos="fade-in" data-aos-delay="400"></i><i class="fa fa-star" data-aos="fade-in" data-aos-delay="500"></i></p>
	          <p class="mimic-h2 section-title mb-4 smaller" data-aos="fade-in">What our clients say...</p>
	          
	          <p class="mb-3 pb-2 position-relative" data-aos="fade-in"><i>"{{$landingpage->testimonial}}"</i></p>
	          <p class="mb-4"><b>{{$landingpage->testimonial_name}} - <a href="/work/{{$landingpage->testimonial_routerlink}}">{{$landingpage->testimonial_project}}</a></b></p>
	          <cool-button :link="'/contact'" :color="'#ffffff'" :text="'Get in touch'" data-aos="fade-up"></cool-button>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="container wide-container">
	  <div class="row justify-content-center position-relative">
	    <div class="col-lg-5 offset-lg-1 pt-5 pb-3 mob-py-0 py-5 text-center text-lg-left">
	      <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink">{{$landingpage->fancy_2}}</b></p>
	      <h2 class="mb-4 smaller" data-aos="fade-in">{{$landingpage->title_2}}</h2>
	      <div class="mb-4 lp-text text-large">{!!$landingpage->text_2!!}</div>
	      <cool-button :link="'/contact'" :color="'#fff'" :text="'Contact Us'" data-aos="fade-up"></cool-button>
	    </div>
	    <div class="col-lg-6 pl-5 mob-px-3 py-5 mob-pb-0">
	    	<picture> 
	      	<source srcset="{{$landingpage->landingpage2}}" type="{{$landingpage->landingpage2mime}}"/> 
          <source srcset="{{$landingpage->landingpage2webp}}" type="image/webp"/>
          <img src="{{$landingpage->landingpage2}}" type="{{$landingpage->landingpage2mime}}" alt="{{$landingpage->title}} featured image" class="w-100"  data-aos="fade-in"/>
        </picture>       
	    </div>
	  </div>
	</div>
	<div class="container">
	  <div class="row pb-5 mob-py-0">
	    <div class="col-lg-12 mob-mt-0 blog-body">
	    	<p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative" data-aos="fade-in"><b class="text-pink">Still not convinced?</b></p>
	      <h2 class="mb-4 smaller" data-aos="fade-in">More about {{$landingpage->title}}</h2>
	      <div class="lp-text lp_body">{!!$landingpage->body!!}</div>
	      <div class="fb-like mt-5" data-href="/work/{{$landingpage->slug}}" data-width="" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
	    </div>
	  </div>
	</div>
	<div class="container-fluid pb-5 position-relative">
	  <div class="dots-line-bottom-right"></div>
	  <div class="row py-5">
	    <div class="container position-relative">
	    	
	      <div class="row">
	        <div class="col-12 mb-4 mob-mb-0">
	          <p class="text-pink mb-0 letter-spacing text-fancy text-large position-relative"><a href="/work"><i class="fa fa-link mr-2 cursor-pointer"></i><b class="text-pink cursor-pointer">Check out more of our work</b></a></p>
	          <h3 class="mb-4 text-white mb-4">Related Work</h3>
	        </div>
	      </div>
	      <div class="row position-relative">
	      	<div class="blue-dots-top-right d-none d-lg-block" data-aos="slide-left"></div>
      		<div class="pink-pentagon-top-right d-none d-lg-block" data-aos="slide-up" data-aos-delay="200"></div>
      		@foreach($landingpage->projects as $other)
	        <div class="col-lg-6">
	        	<a href="/work/{{$other->slug}}">
	            <div class="zoom-link" data-aos="fade-in">
	              <div class="blog-img position-relative">
	              	<picture>
			              <source srcset="{{$other->image}}" type="{{$other->mime}}"/> 
			              <source srcset="{{$other->webp}}" type="image/webp"/>
			              <img src="{{$other->image}}" type="{{$other->mime}}" alt="{{$other->title}}" class="w-100" />
			            </picture>
			            <div class="pointer"><span></span></div>
	              </div>
	              <p class="text-primary mt-3 mb-0 letter-spacing text-fancy text-large position-relative">{{$other->category->name}}</p>
	              <h5 class="text-white">{{$other->title}}</h5>
	              <p class="text-white text-small">{{$other->excerpt}}</p>
	            </div>
	          </a>
	        </div>
	        @endforeach
	      </div>
	    </div>
	  </div>
	</div>
</div>
@endsection
@section('scripts')
@endsection