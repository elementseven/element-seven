(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["ScrollMenu"],{

/***/"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/ScrollMenu.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/ScrollMenu.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************/
/***/function node_modulesBabelLoaderLibIndexJsClonedRuleSet50Rules0Use0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsMenusScrollMenuVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__={
props:{
pageType:String},

data:function data(){
return {};
},
mounted:function mounted(){
var targetElement=document.querySelector('#scroll-menu');
$("#scroll-menu-btn").click(function(){
$("#scroll-menu").toggleClass("active");
$("#scroll-menu-btn-icon").toggleClass("cross");
$('#scroll-mobile-menu-inner').toggleClass('on');
$('#scroll-mobile-menu-trans').toggleClass('on');
});
},
updated:function updated(){},
methods:{
backToTop:function backToTop(){
$("#scroll-menu").toggleClass("active");
$("#scroll-menu-btn-icon").toggleClass("cross");
$('#scroll-mobile-menu-inner').toggleClass('on');
$('#scroll-mobile-menu-trans').toggleClass('on');
setTimeout(function(){
document.body.scrollTop=document.documentElement.scrollTop=0;
},250);
}}};



/***/},

/***/"./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/lib/loader.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/sass-loader/lib/loader.js??clonedRuleSet-22[0].rules[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/ScrollMenu.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/lib/loader.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/sass-loader/lib/loader.js??clonedRuleSet-22[0].rules[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/ScrollMenu.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesCssLoaderIndexJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesPostcssLoaderSrcIndexJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderLibLoaderJsClonedRuleSet120Rules0Use3Node_modulesSassLoaderLibLoaderJsClonedRuleSet220Rules0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsMenusScrollMenuVueVueTypeStyleIndex0LangScss(module,exports,__webpack_require__){

exports=module.exports=__webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */"./node_modules/css-loader/lib/css-base.js")(false);
// imports
exports.push([module.id,"@import url(https://fonts.googleapis.com/css2?family=Caveat:wght@700&family=Noto+Sans&family=Teko:wght@500&display=swap);",""]);

// module
exports.push([module.id,"@-webkit-keyframes colors {\n0% {\n    background-position: 0% 50%;\n}\n50% {\n    background-position: 100% 50%;\n}\n100% {\n    background-position: 0% 50%;\n}\n}\n@-moz-keyframes colors {\n0% {\n    background-position: 0% 50%;\n}\n50% {\n    background-position: 100% 50%;\n}\n100% {\n    background-position: 0% 50%;\n}\n}\n@keyframes colors {\n0% {\n    background-position: 0% 50%;\n}\n50% {\n    background-position: 100% 50%;\n}\n100% {\n    background-position: 0% 50%;\n}\n}\n#scroll-menu {\n  background-color: #090919;\n  position: fixed;\n  top: -150px;\n  left: 0;\n  z-index: 200;\n  width: 100%;\n  height: 98px;\n  opacity: 0;\n  -webkit-box-shadow: 0rem -1rem 3rem #4d05cc !important;\n  -moz-box-shadow: 0rem -1rem 3rem #4d05cc !important;\n  box-shadow: 0rem -1rem 3rem #4d05cc !important;\n  -webkit-transition: all 300ms ease;\n  -moz-transition: all 300ms ease;\n  -o-transition: all 300ms ease;\n  transition: all 300ms ease;\n}\n#scroll-menu .menu-links .menu-item {\n  display: inline-block;\n  margin-left: 2.5rem;\n  cursor: pointer;\n  color: #fff;\n  font-family: \"Noto Sans\", sans-serif;\n  font-weight: 700;\n  font-size: 1rem;\n  -webkit-transition: all 300ms ease;\n  -moz-transition: all 300ms ease;\n  -o-transition: all 300ms ease;\n  transition: all 300ms ease;\n}\n#scroll-menu .menu-links .menu-item a {\n  color: #fff;\n}\n#scroll-menu .menu-links .menu-item a:hover {\n  color: #00daff;\n}\n#scroll-menu .menu-links .menu-item i {\n  font-size: 1.2rem;\n  margin-left: 10px;\n}\n#scroll-menu .menu-links .menu-item:hover, #scroll-menu .menu-links .menu-item.active {\n  color: #00daff !important;\n}\n#scroll-menu .menu_logo {\n  width: 110px;\n}\n#scroll-menu.on {\n  top: -150px;\n  opacity: 1;\n}\n#scroll-menu.active {\n  top: 0;\n}\n#scroll-menu-btn {\n  position: fixed;\n  top: 1rem;\n  right: 1rem;\n  z-index: 10000;\n  width: 50px;\n  height: 50px;\n  overflow: hidden;\n  background-color: #4d05cc;\n  border-radius: 100%;\n}\n#scroll-menu-btn-icon {\n  position: absolute;\n  left: calc(50% - 37px);\n  top: calc(50% - 37px);\n  width: 70px;\n  height: 70px;\n  cursor: pointer;\n  -webkit-transform: translate3d(0, 0, 0);\n  -moz-transform: translate3d(0, 0, 0);\n  -o-transform: translate3d(0, 0, 0);\n  -ms-transform: translate3d(0, 0, 0);\n  transform: translate3d(0, 0, 0);\n}\n#scroll-menu-btn-icon path {\n  fill: none;\n  -webkit-transition: stroke-dashoffset 0.5s cubic-bezier(0.25, -0.25, 0.75, 1.25), stroke-dasharray 0.5s cubic-bezier(0.25, -0.25, 0.75, 1.25);\n  -moz-transition: stroke-dashoffset 0.5s cubic-bezier(0.25, -0.25, 0.75, 1.25), stroke-dasharray 0.5s cubic-bezier(0.25, -0.25, 0.75, 1.25);\n  -o-transition: stroke-dashoffset 0.5s cubic-bezier(0.25, -0.25, 0.75, 1.25), stroke-dasharray 0.5s cubic-bezier(0.25, -0.25, 0.75, 1.25);\n  -ms-transition: stroke-dashoffset 0.5s cubic-bezier(0.25, -0.25, 0.75, 1.25), stroke-dasharray 0.5s cubic-bezier(0.25, -0.25, 0.75, 1.25);\n  transition: stroke-dashoffset 0.5s cubic-bezier(0.25, -0.25, 0.75, 1.25), stroke-dasharray 0.5s cubic-bezier(0.25, -0.25, 0.75, 1.25);\n  stroke-width: 40px;\n  stroke-linecap: round;\n  stroke: #fff;\n  stroke-dashoffset: 0px;\n}\npath#scroll-menu-btn-top,\npath#scroll-menu-btn-bottom {\n  stroke-dasharray: 240px 950px;\n}\npath#scroll-menu-btn-middle {\n  stroke-dasharray: 240px 240px;\n}\n#scroll-menu-btn-icon.cross path#scroll-menu-btn-top,\n#scroll-menu-btn-icon.cross path#scroll-menu-btn-bottom {\n  stroke-dashoffset: -650px;\n  stroke-dashoffset: -650px;\n}\n#scroll-menu-btn-icon.cross path#scroll-menu-btn-middle {\n  stroke-dashoffset: -115px;\n  stroke-dasharray: 1px 220px;\n}\n",""]);

// exports


/***/},

/***/"./node_modules/style-loader/index.js!./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/lib/loader.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/sass-loader/lib/loader.js??clonedRuleSet-22[0].rules[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/ScrollMenu.vue?vue&type=style&index=0&lang=scss&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/index.js!./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/lib/loader.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/sass-loader/lib/loader.js??clonedRuleSet-22[0].rules[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/ScrollMenu.vue?vue&type=style&index=0&lang=scss& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesStyleLoaderIndexJsNode_modulesCssLoaderIndexJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesPostcssLoaderSrcIndexJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderLibLoaderJsClonedRuleSet120Rules0Use3Node_modulesSassLoaderLibLoaderJsClonedRuleSet220Rules0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsMenusScrollMenuVueVueTypeStyleIndex0LangScss(module,__unused_webpack_exports,__webpack_require__){


var content=__webpack_require__(/*! !!../../../../node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/lib/loader.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/sass-loader/lib/loader.js??clonedRuleSet-22[0].rules[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ScrollMenu.vue?vue&type=style&index=0&lang=scss& */"./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/lib/loader.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/sass-loader/lib/loader.js??clonedRuleSet-22[0].rules[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/ScrollMenu.vue?vue&type=style&index=0&lang=scss&");

if(typeof content==='string')content=[[module.id,content,'']];

var transform;



var options={"hmr":true};

options.transform=transform;
options.insertInto=undefined;

var update=__webpack_require__(/*! !../../../../node_modules/style-loader/lib/addStyles.js */"./node_modules/style-loader/lib/addStyles.js")(content,options);

if(content.locals)module.exports=content.locals;

/***/},

/***/"./resources/js/components/Menus/ScrollMenu.vue":
/*!******************************************************!*\
  !*** ./resources/js/components/Menus/ScrollMenu.vue ***!
  \******************************************************/
/***/function resourcesJsComponentsMenusScrollMenuVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _ScrollMenu_vue_vue_type_template_id_1ce77ed2___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./ScrollMenu.vue?vue&type=template&id=1ce77ed2& */"./resources/js/components/Menus/ScrollMenu.vue?vue&type=template&id=1ce77ed2&");
/* harmony import */var _ScrollMenu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./ScrollMenu.vue?vue&type=script&lang=js& */"./resources/js/components/Menus/ScrollMenu.vue?vue&type=script&lang=js&");
/* harmony import */var _ScrollMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! ./ScrollMenu.vue?vue&type=style&index=0&lang=scss& */"./resources/js/components/Menus/ScrollMenu.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__=__webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");


/* normalize component */

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
_ScrollMenu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
_ScrollMenu_vue_vue_type_template_id_1ce77ed2___WEBPACK_IMPORTED_MODULE_0__.render,
_ScrollMenu_vue_vue_type_template_id_1ce77ed2___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/Menus/ScrollMenu.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/Menus/ScrollMenu.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/Menus/ScrollMenu.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/***/function resourcesJsComponentsMenusScrollMenuVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollMenu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ScrollMenu.vue?vue&type=script&lang=js& */"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/ScrollMenu.vue?vue&type=script&lang=js&");
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollMenu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default;

/***/},

/***/"./resources/js/components/Menus/ScrollMenu.vue?vue&type=style&index=0&lang=scss&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Menus/ScrollMenu.vue?vue&type=style&index=0&lang=scss& ***!
  \****************************************************************************************/
/***/function resourcesJsComponentsMenusScrollMenuVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony import */var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_lib_loader_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_sass_loader_lib_loader_js_clonedRuleSet_22_0_rules_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/style-loader/index.js!../../../../node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/lib/loader.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/sass-loader/lib/loader.js??clonedRuleSet-22[0].rules[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ScrollMenu.vue?vue&type=style&index=0&lang=scss& */"./node_modules/style-loader/index.js!./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/lib/loader.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/sass-loader/lib/loader.js??clonedRuleSet-22[0].rules[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/ScrollMenu.vue?vue&type=style&index=0&lang=scss&");
/* harmony reexport (unknown) */var __WEBPACK_REEXPORT_OBJECT__={};
/* harmony reexport (unknown) */var _loop=function _loop(__WEBPACK_IMPORT_KEY__){if(__WEBPACK_IMPORT_KEY__!=="default")__WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__]=function(){return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_lib_loader_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_sass_loader_lib_loader_js_clonedRuleSet_22_0_rules_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[__WEBPACK_IMPORT_KEY__];};};for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_lib_loader_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_sass_loader_lib_loader_js_clonedRuleSet_22_0_rules_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__){_loop(__WEBPACK_IMPORT_KEY__);}
/* harmony reexport (unknown) */__webpack_require__.d(__webpack_exports__,__WEBPACK_REEXPORT_OBJECT__);


/***/},

/***/"./resources/js/components/Menus/ScrollMenu.vue?vue&type=template&id=1ce77ed2&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/Menus/ScrollMenu.vue?vue&type=template&id=1ce77ed2& ***!
  \*************************************************************************************/
/***/function resourcesJsComponentsMenusScrollMenuVueVueTypeTemplateId1ce77ed2(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollMenu_vue_vue_type_template_id_1ce77ed2___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollMenu_vue_vue_type_template_id_1ce77ed2___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollMenu_vue_vue_type_template_id_1ce77ed2___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ScrollMenu.vue?vue&type=template&id=1ce77ed2& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/ScrollMenu.vue?vue&type=template&id=1ce77ed2&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/ScrollMenu.vue?vue&type=template&id=1ce77ed2&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/ScrollMenu.vue?vue&type=template&id=1ce77ed2& ***!
  \****************************************************************************************************************************************************************************************************************************/
/***/function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsMenusScrollMenuVueVueTypeTemplateId1ce77ed2(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c(
"div",
{
staticClass:"menu position-fixed d-none d-lg-block",
attrs:{id:"scroll-menu"}},

[
_c("div",{staticClass:"container-fluid mob-px-3 px-5"},[
_c("div",{staticClass:"row"},[
_c(
"div",
{staticClass:"col-2 col-lg-2 pt-3"},
[
_c(
"router-link",
{
attrs:{to:{name:"Welcome"}},
nativeOn:{
click:function click($event){
return _vm.backToTop();
}}},


[
_vm.pageType=="dark"?
_c("img",{
staticClass:"menu_logo",
attrs:{
src:"/img/logos/logo-full-grad.svg",
alt:"Element Seven Logo"}}):


_c("img",{
staticClass:"menu_logo",
attrs:{
src:"/img/logos/logo-full-grad.svg",
alt:"Element Seven Logo"}})])],





1),

_vm._v(" "),
_c(
"div",
{
staticClass:
"col-lg-10 pt-4 mt-2 pr-5 text-right d-none d-lg-block"},

[
_c(
"div",
{staticClass:"menu-links d-block "},
[
_c(
"router-link",
{
attrs:{to:{name:"Work"}},
nativeOn:{
click:function click($event){
return _vm.backToTop();
}}},


[
_c(
"span",
{
staticClass:"menu-item cursor-pointer",
class:
_vm.pageType=="dark"?"text-dark":"text-white"},

[_vm._v("Work")])]),



_vm._v(" "),
_c(
"router-link",
{
attrs:{to:{name:"Services"}},
nativeOn:{
click:function click($event){
return _vm.backToTop();
}}},


[
_c(
"span",
{
staticClass:"menu-item cursor-pointer",
class:
_vm.pageType=="dark"?"text-dark":"text-white"},

[_vm._v("Services")])]),



_vm._v(" "),
_c(
"router-link",
{
attrs:{to:{name:"Blog"}},
nativeOn:{
click:function click($event){
return _vm.backToTop();
}}},


[
_c(
"span",
{
staticClass:"menu-item cursor-pointer ",
class:
_vm.pageType=="dark"?"text-dark":"text-white"},

[_vm._v("Blog")])]),



_vm._v(" "),
_c(
"router-link",
{
attrs:{to:{name:"Contact"}},
nativeOn:{
click:function click($event){
return _vm.backToTop();
}}},


[
_c(
"span",
{
staticClass:"menu-item cursor-pointer",
class:
_vm.pageType=="dark"?"text-dark":"text-white"},

[_vm._v("Contact")])]),



_vm._v(" "),
_c(
"a",
{
staticClass:"mr-4",
class:
_vm.pageType=="dark"?"text-dark":"text-white",
attrs:{
href:"https://www.facebook.com/elementsevendigital/",
rel:"noreferrer",
target:"_blank",
"aria-label":"Element Seven Facebook"}},


[_c("i",{staticClass:"fa fa-facebook ml-5"})]),

_vm._v(" "),
_c(
"a",
{
staticClass:"mr-4",
class:
_vm.pageType=="dark"?"text-dark":"text-white",
attrs:{
href:"https://www.instagram.com/elementsevendigital/",
rel:"noreferrer",
target:"_blank",
"aria-label":" Element Seven Instagram"}},


[_c("i",{staticClass:"fa fa-instagram"})]),

_vm._v(" "),
_c(
"a",
{
staticClass:"mr-4",
class:
_vm.pageType=="dark"?"text-dark":"text-white",
attrs:{
href:"https://twitter.com/elementsevenco/",
rel:"noreferrer",
target:"_blank",
"aria-label":" Element Seven Twitter"}},


[_c("i",{staticClass:"fa fa-twitter"})]),

_vm._v(" "),
_c(
"a",
{
staticClass:"mr-4",
class:
_vm.pageType=="dark"?"text-dark":"text-white",
attrs:{
href:"https://www.linkedin.com/company/element-seven/",
rel:"noreferrer",
target:"_blank",
"aria-label":" Element Seven Linkedin"}},


[_c("i",{staticClass:"fa fa-linkedin"})])],


1)])])]),





_vm._v(" "),
_c("div",{attrs:{id:"scroll-menu-btn"}},[
_c(
"svg",
{attrs:{id:"scroll-menu-btn-icon",viewBox:"0 0 800 600"}},
[
_c("path",{
attrs:{
d:
"M300,220 C300,220 520,220 540,220 C740,220 640,540 520,420 C440,340 300,200 300,200",
id:"scroll-menu-btn-top"}}),


_vm._v(" "),
_c("path",{
attrs:{d:"M300,320 L540,320",id:"scroll-menu-btn-middle"}}),

_vm._v(" "),
_c("path",{
attrs:{
d:
"M300,210 C300,210 520,210 540,210 C740,210 640,530 520,410 C440,330 300,190 300,190",
id:"scroll-menu-btn-bottom",
transform:
"translate(480, 320) scale(1, -1) translate(-480, -318) "}})])])]);







};
var _staticRenderFns=[];
_render._withStripped=true;



/***/}}]);

}());
